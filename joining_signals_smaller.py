
#%%
from re import T
import ROOT
#from os import read
import os
import errno
import sys
import tables  #convertion hdf
from numpy.core.fromnumeric import trace
print(sys.version)
import csv
import pandas as pd
import matplotlib.pyplot as plt
import uproot
import awkward as ak
#from root_pandas  import read_root
import numpy as np
import h5py
import shutil
import pickle
from tqdm import tqdm
from scipy.signal import find_peaks
#for i in tqdm(range(10)):
#with open('gamma_signal.csv') as File:  
#    reader = csv.reader(File)
#    for row in reader:
#        print(row)  nfs/neutron-ml /nfs/neutron-ml/event.#%%
#reading the file

total_events = 9257893
sel_channel = 1
max_delay = 12
new_df = pd.read_hdf('/nfs/neutron-ml/temp/test_ch' + str(sel_channel) + '_iteration_0_events.h5')

#print ('new_df')
#print(new_df)
print('merging')
for y in tqdm(range(1, total_events)):  #first signal
       next_df = pd.read_hdf('/nfs/neutron-ml/temp/test_ch' + str(sel_channel) + '_iteration_' + str(y) +'_events.h5')
       #print ('next_df')
       #print(next_df)
       new_df = pd.concat([new_df, next_df], ignore_index=True)
       #print ('new_df2')
       #print(new_df)
#%%purchase
#pd.to_numeric(new_df)

new_df['BoardId'] = new_df['BoardId'].astype('int')

new_df['ChannelId'] = new_df['ChannelId'].astype('int')
new_df['TimeStamp1'] = new_df['TimeStamp1'].astype('int')
new_df['TimeStamp2'] = new_df['TimeStamp2'].astype('int')
new_df['Delay'] = new_df['Delay'].astype('int')
#new_df['Trace1'] = new_df['Trace1'].astype('int')


new_df.dtypes
#new_df = new_df.apply(pd.to_numeric, errors='ignore')

new_df.info()

final_events  = new_df.size
#new_df = new_df.convert_objects(convert_numeric=True)
path = '/nfs/neutron-ml/test_ch' + str(sel_channel) + '_merged_' + str(final_events) + 'events_' + str(max_delay) + 'delays.pickle'

new = new_df.to_pickle(path)
#
#path = '/nfs/neutron-ml/test_ch0_merged_events.ftr'
#feather.write_feather(new_df, path, compression='uncompressed')
#new_df = feather.read_feather(path)


#print (new_df)
#%%


read_df = pd.read_pickle(path)

print('reading pickle')
read_df.info()
#%%



#new_df.to_hdf('/nfs/neutron-ml/test_ch0_merged_events.h5', key='new_df', mode='w')  #function to convert and save in .h5
#reread = pd.read_hdf('/nfs/neutron-ml/ch0_Xevents.h5')
print('done')
#total_artificial_events = len(reread.index)

#print  ("total_artificial_events in ch0_Xevents.h5")

#print (total_artificial_events)


#print (df_final)
#type(df_final)

#hf = h5py.File('/nfs/neutron-ml/test.h5', 'w')

#with h5py.File('/nfs/neutron-ml/test.h5', 'w') as f:
#    dset = f.create_dataset("default", data = df_2_signals)



#%%

