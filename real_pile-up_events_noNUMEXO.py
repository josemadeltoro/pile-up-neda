#%%
from re import T
import ROOT
#from os import read
import os
import errno
import sys
import tables  #convertion hdf
from numpy.core.fromnumeric import trace
print(sys.version)
import csv
import pandas as pd
import matplotlib.pyplot as plt
import uproot
import awkward as ak
#from root_pandas  import read_root
import numpy as np
#import h5py
import shutil
import pickle
from tqdm import tqdm
from scipy.signal import find_peaks
#for i in tqdm(range(10)):
#with open('gamma_signal.csv') as File:  
#    reader = csv.reader(File)
#    for row in reader:
#        print(row)  nfs/neutron-ml /nfs/neutron-ml/event.root
#%%
#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"]="0"

df_final = pd.DataFrame()
df_final = pd.DataFrame(columns=['Channel', 'Timestamp', 'Board', 'Trace', 'Baseline'])
#df_final['BoardId'] = None
#df_final['ChannelId'] = None
#df_final['TimeStamp1'] = None
#df_final['TimeStamp2'] = None
#df_final['Trace1'] = None
#df_final['Trace2'] = None
#df_final['TraceFinal'] = None
#df_final['Delay'] = None


#print("df_final")
#print(df_final)



print("Loading File")
data = pd.read_pickle("Data_test_03_2Mevents.pickle")
#%%
print(len(data))
print("Taking pile-up signals")
for i in tqdm(range(0, len(data))):
    trace_current = np.array(data.loc[i]['Trace'])
    
    #print(current_trace)

    
    baseline = round (np.mean(trace_current[:20]))

    current_trace = np.append(trace_current,trace_current[:26])
    current_trace = np.append(current_trace,trace_current[:26])
    current_trace = np.append(current_trace,trace_current[:26])
    current_trace = np.append(current_trace,trace_current[:26])
    current_trace = np.roll(current_trace, 10)

    trace_final = (trace_current - baseline)
    traceFinal_inv = (trace_final*-1)
    #print (traceFinal_inv.shape)
    #traceFinal_inv = traceFinal_inv.transpose()
    #print(traceFinal_inv)
    peaks = find_peaks(traceFinal_inv, height= 500, distance = 4, prominence=200)
    #print (peaks[0])
    
    if 3>len(peaks[0])>1 :
        if ((peaks[0][1] - peaks[0][0])<13) and ( 38 <peaks[0][0]< 46):
            #print("hello")
            #plt.plot(current_trace)
            #plt.show()
            df_final = df_final.append({'Channel': data.loc[i]['Channel'], 'Timestamp':data.loc[i]['Timestamp'], 'Board':data.loc[i]['Board'],'Trace':current_trace, 'Baseline': baseline}, ignore_index=True)

#%%
#print(df_final)
path = 'data_only_pile-up_noNUMEXO.pickle'
print("Creating pickle file")
df_final.to_pickle(path)
            #print("peak0")
        
# %%
