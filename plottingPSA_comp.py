# %%
from re import T
import ROOT
#from os import read
import os
import errno
import sys
import tables  #convertion hdf
from numpy.core.fromnumeric import trace
print(sys.version)
import csv
import pandas as pd
import matplotlib.pyplot as plt
import uproot
import awkward as ak
#from root_pandas  import read_root
import numpy as np
import h5py
import shutil
import pickle
from tqdm import tqdm
from scipy.signal import find_peaks
#for i in tqdm(range(10)):
#with open('gamma_signal.csv') as File:  
#    reader = csv.reader(File)
#    for row in reader:
#        print(row)  nfs/neutron-ml /nfs/neutron-ml/event.root

# %%
path = '/eos/home-m/mlphd/results_PSA_comparison_ch10_100000events_1stsignal.pickle'
read_df = pd.read_pickle(path)

# %%
'''
from turtle import color


fig2 = plt.figure(figsize=(10,6), dpi=600)      
plt.xlabel("samples")
plt.ylabel("value")
plt.hist(read_df['Slow/Fast_result'], bins = 600)

#plt.plot(np.array(range(232)),trace1[eventNumber].reshape(232))
#plt.plot(np.array(range(232)),trace2[eventNumber].reshape(232))

plt.xlim(0,0.5)

plt.legend(['Slow/Fast_result'])
plt.show()


fig2 = plt.figure(figsize=(10,6), dpi=600)      
plt.xlabel("samples")
plt.ylabel("value")
plt.hist(read_df['Slow/Fast_orig'], bins = 300, color= 'r')

#plt.plot(np.array(range(232)),trace1[eventNumber].reshape(232))
#plt.plot(np.array(range(232)),trace2[eventNumber].reshape(232))

#plt.xlim(0,0.5)

plt.legend(['Slow/Fast_orig'])
plt.show()'''

from sklearn.metrics import mean_squared_error, mean_absolute_error
y_true = read_df['Slow/Fast_orig']
y_pred = read_df['Slow/Fast_result']


mse = mean_squared_error(y_true, y_pred)
#mae = mean_absolute_error(y_true, y_pred)
#print ("Mean absolute error: " + str(mae))
print ("TOTAL Mean squared error: " + str(mse))

i =3
for i in range(3, 40):
    new_df = read_df.loc[read_df.loc[:, 'Delay'] == i]
    y_true = new_df['Slow/Fast_orig']
    y_pred = new_df['Slow/Fast_result']
    mse = round(mean_squared_error(y_true, y_pred),4)
    mae = round(mean_absolute_error(y_true, y_pred),4)
    print ("DELAY:"+ str(i))
    print ("Mean squared error: " + str(mse) + "\n")

#print(read_df)



# %%
