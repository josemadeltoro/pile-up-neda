## Network definition:
import torch
from torch import nn
import torch.nn.functional as F
import copy
class AutoNet(nn.Module):
    def __init__(self):
      super(AutoNet, self).__init__()
      self.conv1 = nn.Conv1d(in_channels=1, out_channels=16, kernel_size=3, stride=1, padding=1)
      self.conv2 = nn.Conv1d(in_channels=16, out_channels=32, kernel_size=3, stride=1, padding=1)
      # self.dropout1 = nn.Dropout1d(0.25)
      # self.dropout2 = nn.Dropout1d(0.5)
      self.fc1 = nn.Linear(7424, 1024)

      #Branch 1
      self.fc21 = nn.Linear(1024, 7424)

      self.conv31 = nn.Conv1d(in_channels=32, out_channels=16, kernel_size=3, stride=1, padding=1)
      self.conv41 = nn.Conv1d(in_channels=16, out_channels=1, kernel_size=3, stride=1, padding=1)

      #Branch 2
      self.fc22 = nn.Linear(1024, 7424)

      self.conv32 = nn.Conv1d(in_channels=32, out_channels=16, kernel_size=3, stride=1, padding=1)
      self.conv42 = nn.Conv1d(in_channels=16, out_channels=1, kernel_size=3, stride=1, padding=1)

    # x represents our data
    def forward(self, x):
      # Pass data through conv1
      x = self.conv1(x)
      # Use the rectified-linear activation function over x
      # print("Conv1: ", x.shape)
      x = F.relu(x)
      # print("ReLU 1: ", x.shape)
      x = self.conv2(x)
      # print("Conv2: ", x.shape)
      x = F.relu(x)
      # print("ReLu 2: ", x.shape)

      # Run max pooling over x
      # x = F.max_pool1d(x, 2)
      # print("MaxPool: ", x.shape)
      # Pass data through dropout1
      # x = self.dropout1(x)


      # Flatten x with start_dim=1
      x = torch.flatten(x, 1)
      # print("Flatten: ", x.shape)
      # Pass data through fc1
      x = self.fc1(x)
      # print("FC1: ", x.shape)
      x = F.relu(x)
      # print("ReLu 3: ", x.shape)
      # x = self.dropout2(x)

      # Branch 1
      x1 = self.fc21(x)
      # print("FC2: ", x1.shape)
      x1 = torch.reshape(x1,(-1,32,232))
      x1 = self.conv31(x1)
      # print("Conv3: ", x1.shape)
      x1 = F.relu(x1)
      # print("ReLU 4: ", x1.shape)
      x1 = self.conv41(x1)
      # print("Conv4: ", x1.shape)
      o1 = F.relu(x1)

      # Branch 2
      x2 = self.fc22(x)
      # print("FC2: ", x2.shape)
      x2 = torch.reshape(x2,(-1,32,232))
      x2 = self.conv32(x2)
      # print("Conv3: ", x2.shape)
      x2 = F.relu(x2)
      # print("ReLU 4: ", x2.shape)
      x2 = self.conv42(x2)
      # print("Conv4: ", x2.shape)
      o2 = F.relu(x2)

      # Apply softmax to x
      # output = F.log_softmax(x, dim=1)
      return o1#,o2
