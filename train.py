# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import normalize
import numpy as np
import torch
import torch.nn as nn
from torch.autograd.variable import *
import torch.optim as optim
import torch.nn.functional as F
from matplotlib import pyplot as plt
from tqdm import tqdm, trange



# %%

## First we check if CUDA is available on this server. Otherwise run on CPU
device = 'cpu'
args_cuda = torch.cuda.is_available()
if args_cuda: device = "cuda"
print(device)

# %%

## We can have all the data in memory so far, so, we just load the data in one go.
print("Loading data...")
df = pd.read_hdf('/nfs/neutron-ml/test2.h5')

## And we normalize it:
print('Normalizing data..')
input_signal = normalize(np.stack(df['TraceFinal'].values))
out1 = normalize(np.stack(df['Trace1'].values))
#out2 = normalize(np.stack(df['Trace2'].values))

## We split the data in train + validation sets:
#X_train, X_val, y1_train, y1_val, y2_train, y2_val  = train_test_split(input_signal, out1, test_size=0.25)
X_train, X_val, y1_train, y1_val  = train_test_split(input_signal, out1, test_size=0.25)

print(X_train.shape, X_val.shape, y1_train.shape, y1_val.shape)

##  And we convert it to torch vectors
X_train = Variable(torch.FloatTensor(X_train)).to(device)
X_val = Variable(torch.FloatTensor(X_val)).to(device)
y1_train = Variable(torch.FloatTensor(y1_train)).to(device)
y1_val = Variable(torch.FloatTensor(y1_val)).to(device)
#y2_train = Variable(torch.FloatTensor(y2_train)).to(device)
#y2_val = Variable(torch.FloatTensor(y2_val)).to(device)


# %%

## We define some training parameters
n_epochs = 30
batch_size = 128
patience =  10


# %%

# This is the training part:
from NN import AutoNet


an = AutoNet()
an.to(device)
# loss = nn.CrossEntropyLoss()
# loss = nn.L1Loss()
loss = nn.MSELoss()
optimizer = optim.Adam(an.parameters(), lr = 0.01, weight_decay=0.01)

loss_train = np.zeros(n_epochs)
acc_train = np.zeros(n_epochs)
loss_val = np.zeros(n_epochs)
acc_val = np.zeros(n_epochs)

for i in range(n_epochs):
    print("Epoch %s" % i, flush=True)
    for j in trange(0, X_train.size()[0], batch_size):
        optimizer.zero_grad()
        out1 = an(X_train[j:j + batch_size].unsqueeze(0).swapaxes(0, 1))
        target1 = y1_train[j:j + batch_size].unsqueeze(0).swapaxes(0, 1)
        #target2 = y2_train[j:j + batch_size].unsqueeze(0).swapaxes(0, 1)
        # print("Target:", target1,target2)
        l1 = loss(out1, target1)
        #l2 = loss(out2, target2)
        torch.autograd.backward(l1)
        # print("Loss 1", l1.cpu().detach().numpy())
        # print("Loss 2", l2.cpu().detach().numpy())

        optimizer.step()
        loss_train[i] += (l1).cpu().data.numpy()*batch_size
    loss_train[i] = loss_train[i]/X_train.shape[0]
    #acc_train[i] = stats(predicted, Y_val)
    #### val loss & accuracy
    print("Validation steps:", flush=True)
    for j in trange(0, X_val.size()[0], batch_size):
        out_val1 = an(X_val[j:j + batch_size].unsqueeze(0).swapaxes(0, 1))
        target_val1 =  y1_val[j:j + batch_size].unsqueeze(0).swapaxes(0, 1)
        #target_val2 =  y2_val[j:j + batch_size].unsqueeze(0).swapaxes(0, 1)
        
        l_val1 = loss(out_val1,target_val1)
        # print("Loss 1", l_val1.cpu().detach().numpy())
        #l_val2 = loss(out_val2,target_val2)
        # print("Loss 2: ", l_val2.cpu().detach().numpy())
        l_val = l_val1# + l_val2)
        loss_val[i] += l_val.cpu().data.numpy()*batch_size 
    loss_val[i] = loss_val[i]/X_val.shape[0]
    print("Training   Loss: %f" %(l1).cpu().data.numpy())
    print("Validation Loss: %f" %l_val.cpu().data.numpy())
    if all(loss_val[max(0, i - patience):i] > min(np.append(loss_val[0:max(0, i - patience)], 200))) and i > patience:
        print("Early Stopping")
        break
    print("Done")


# %%

## After training we save the model
print('Saving model...')
torch.save(an, "model.torch")
print('Done.')




#%%
df = pd.read_hdf('/nfs/neutron-ml/test2.h5')


input_signal = normalize(np.stack(df['TraceFinal'].values))
#print (input_signal[1])
print (input_signal[1].shape)
#%%


with torch.no_grad():
    model = torch.load("model.torch")
    model.eval()
    x_np = torch.DoubleTensor(input_signal[1]).unsqueeze(0).swapaxes(0, 1)#.reshape(1,232,1)
    #x_np = Variable(torch.from_numpy(input_signal[1]))
    print (x_np)
    input = x_np
   
    print(input.shape)

    result = model(input)

    fig = plt.figure()      
    plt.xlabel("samples")
    plt.ylabel("value")
#plt.ylim(0,15000)
    plt.scatter(np.array(range(232)),result.reshape(232), s=1)
#plt.show()
    fig.savefig('ResultRNN.png')

 # %%
