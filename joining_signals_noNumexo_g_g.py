
#%%
from re import T
import ROOT
#from os import read
import os
import errno
import sys
import tables  #convertion hdf
from numpy.core.fromnumeric import trace
print(sys.version)
import csv
import pandas as pd
import matplotlib.pyplot as plt
import uproot
import awkward as ak
#from root_pandas  import read_root
import numpy as np
import h5py
import shutil
import pickle
from tqdm import tqdm
from scipy.signal import find_peaks
import random
#for i in tqdm(range(10)):
#with open('gamma_signal.csv') as File:  
#    reader = csv.reader(File)
#    for row in reader:
#        print(row)  nfs/neutron-ml /nfs/neutron-ml/event.root
#%%
#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"]="0"

df_final = pd.DataFrame()

#df_final['BoardId'] = None
#df_final['ChannelId'] = None
#df_final['TimeStamp1'] = None
#df_final['TimeStamp2'] = None
#df_final['Trace1'] = None
#df_final['Trace2'] = None
#df_final['TraceFinal'] = None
#df_final['Delay'] = None


#print("df_final")
#print(df_final)
#print(df_final.dtypes)
#path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_A.pickle')
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_A_2nd_dataset_E.pickle')
df_origA = pd.read_pickle(path)
df_origA = df_origA.loc[df_origA.loc[:, 'Slow/Fast_orig'] > 0.6]
df_origA = df_origA.reset_index(drop=True)
print(df_origA)

#path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_B.pickle')
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_B_2nd_dataset_E.pickle')
df_origB = pd.read_pickle(path)
df_origB = df_origB.loc[df_origB.loc[:, 'Slow/Fast_orig'] > 0.6]
df_origB = df_origB.reset_index(drop=True)
print(df_origB)

#path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_C.pickle')
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_A_2nd_dataset_F.pickle')
df_origC = pd.read_pickle(path)
df_origC = df_origC.loc[df_origC.loc[:, 'Slow/Fast_orig'] > 0.6]
df_origC = df_origC.reset_index(drop=True)
print(df_origC)

#path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_D.pickle')
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_B_2nd_dataset_F.pickle')
df_origD = pd.read_pickle(path)
df_origD = df_origD.loc[df_origD.loc[:, 'Slow/Fast_orig'] > 0.6]
df_origD = df_origD.reset_index(drop=True)
print(df_origD)

df_orig = pd.concat([df_origA, df_origB, df_origC, df_origD], ignore_index=1)
###gammas

#path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_A.pickle')
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_A_2nd_dataset_E.pickle')
df_gA = pd.read_pickle(path)
df_gA = df_gA.loc[df_gA.loc[:, 'Slow/Fast_orig'] < 0.6]
df_gA = df_gA.reset_index(drop=True)
print(df_gA)

#path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_B.pickle')
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_B_2nd_dataset_E.pickle')
df_gB = pd.read_pickle(path)
df_gB = df_gB.loc[df_gB.loc[:, 'Slow/Fast_orig'] < 0.6]
df_gB = df_gB.reset_index(drop=True)
print(df_gB)

#path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_C.pickle')
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_A_2nd_dataset_F.pickle')
df_gC = pd.read_pickle(path)
df_gC = df_gC.loc[df_gC.loc[:, 'Slow/Fast_orig'] < 0.6]
df_gC = df_gC.reset_index(drop=True)
print(df_gC)

#path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_D.pickle')
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_B_2nd_dataset_F.pickle')
df_gD = pd.read_pickle(path)
df_gD = df_gD.loc[df_gD.loc[:, 'Slow/Fast_orig'] < 0.6]
df_gD = df_gD.reset_index(drop=True)
print(df_gD)

df_g= pd.concat([df_gA, df_gB, df_gC, df_gD], ignore_index=1)

#%%
print (type(df_orig))
print (type(df_final))
print (df_orig)
df_init = df_orig
#print (df_traces)
#df_orig.show()

#%%

#df_single_channel = df_init.loc[[0], 'Trace']    

#df_single_channel.info()
#print(df_single_channel)
total_events = len(df_init)
#total_events = round(len(df_init)/2)
#total_events = 200
print(len(df_init))
print('Events mixing:' + str(total_events))
#print (total_events)
start_event = 0#round(len(df_init)/2)
#%%
#########################
#total_events = 2
max_delay = 40
#########################

# Single channel, same baseline

#def zerolistmaker(baseline,n):
#       listofzeros = [baseline] * n
#       return listofzeros


try:
    os.mkdir('/eos/home-m/mlphd/temp2/')
    #os.mkdir('/nfs/neutron-ml/temp/')
except OSError as e:
    if e.errno != errno.EEXIST:
        raise


for j in tqdm(range(0, total_events)):  #first signal
    #print('iteration:' + str(j))        
    #for i in tqdm(range(0, total_events)): #second signal
    i = random.randint(0,total_events)
    delay = random.randint(6,39)
    df_g_single = df_g.iloc[[j]]
    #df_g_single = df_g_single.to_frame()
    #print(df_g_single)
    #print(type(df_g_single))
    df_g_single_rand = df_g.iloc[[i]]
    #df_n_single = df_n_single.to_frame()   
    #print(df_n_single)
    #print(type(df_n_single))
    df_2_signals = pd.concat([df_g_single, df_g_single_rand], ignore_index=1)
    #print(df_2_signals)

    df_2_signals = df_2_signals.reset_index(drop=True)

    Ts1 = df_2_signals.loc[[0], 'Timestamp']
    Ts2 = df_2_signals.loc[1, 'Timestamp']
    Tr1 = df_2_signals.loc[0, 'Trace']
    Tr2 = df_2_signals.loc[1, 'Trace']
    


    trace0 = df_2_signals.loc[0, 'Trace']
    trace1 = df_2_signals.loc[1, 'Trace']

    baseline = round (np.mean(trace1[:20]))
    #array_delay = zerolistmaker(baseline, delay)
    trace1_delayed = np.roll(trace1, delay)
    np.array(trace1_delayed)
    Tr2_delayed = trace1_delayed

    sumstart = 46 + delay - 4                    #trace1_delayed = np.concatenate((array_delay, trace1), axis=None)
    trace1_delayed = trace1_delayed - baseline
    trace1_delayed = trace1_delayed[sumstart:232]
    
    #print (trace1_delayed)
    #print (np.info(trace1_delayed))
    inital_zeros = np.zeros(sumstart)
    #print (inital_zeros)
    #print (np.info(inital_zeros))
    trace1_delayed = np.concatenate((inital_zeros,trace1_delayed))

    #plt.plot(trace1_delayed)
    #plt.show()
    
    trace0 = np.array(trace0)
    #trace1_delayed = np.array(trace1_delayed)
    
    trace_final = (trace0 - baseline) + (trace1_delayed)
    ## cheking trace 2 peaks if delay 3 or 4
    
    #plt.plot(trace_final)
    #plt.show()
    
    if 6>delay>=3 :
        #from scipy.signal import find_peaks
        traceFinal_inv = (trace_final*-1)
        #print (traceFinal_inv.shape)
        traceFinal_inv = traceFinal_inv.transpose()
        #print(traceFinal_inv)
        peaks = find_peaks(traceFinal_inv, height= 100, distance = 3)
        height = peaks[1]['peak_heights']
        #print (height) 
        #print(peaks)
        trace_final = trace_final + baseline
        if len(height)>1 :
            #plt.plot(trace_final)
            #plt.show()
            df_final = df_final.append({'BoardId' : df_2_signals.loc[[0], 'Board'], 'ChannelId1': df_2_signals.loc[[0], 'Channel'],  'ChannelId2': df_2_signals.loc[[1], 'Channel'], 'TimeStamp1':Ts1, 'TimeStamp2':Ts2, 'Trace1':Tr1, 'Trace2':Tr2_delayed,'TraceFinal' : trace_final, 'Delay': delay}, ignore_index=True)

    else:
    ##
        trace_final = trace_final + baseline
        df_final = df_final.append({'BoardId' : df_2_signals.loc[[0], 'Board'], 'ChannelId1': df_2_signals.loc[[0], 'Channel'], 'ChannelId2': df_2_signals.loc[[1], 'Channel'],'TimeStamp1':Ts1, 'TimeStamp2':Ts2, 'Trace1':Tr1, 'Trace2':Tr2_delayed,'TraceFinal' : trace_final, 'Delay': delay}, ignore_index=True)

                    #plt.plot(trace0,"--")
                    #plt.plot(trace1_delayed,"--")
                    #plt.plot(trace_final)
                    #plt.ylabel('values')
                    #plt.show()

#                   if len(height)>1 :
#                       plt.plot(trace_final)
#                       plt.show()
#                       df_final = df_final.append({'BoardId' : df_2_signals.loc[[0], 'BoardId'], 'ChannelId': df_2_signals.loc[[0], 'ChannelId'], 'TimeStamp1':Ts1, 'TimeStamp2':Ts2, 'Trace1':Tr1, 'Trace2':Tr2,'TraceFinal' : trace_final, 'Delay': delay}, ignore_index=True)
                    
                    #print("df_final")

                    #print(df_final)

    #df_final.to_hdf('/nfs/neutron-ml/temp/test_ch' + str(sel_channel) + '_iteration_' + str(j) +'_events.h5', key='df_final', mode='w')  #function to convert and save in .h5
    df_final.to_hdf('/eos/home-m/mlphd/temp2/test_several_channels_iteration_' + str(j) + '_events.h5', key='df_final', mode='w')  #function to convert and save in .h5
    
    #print (df_final)
    #df_final.drop([])
    #print(df_final)
    df_final = df_final.iloc[0:0] #cleaning dataframe
    #print ('cleaned df')
    #print(df_final)
    #df_final.info()

#%%
#%%/eos/home-m/mlphd/
#reading the file
#new_df = pd.read_hdf('/nfs/neutron-ml/temp/test_ch' + str(sel_channel) + '_iteration_0_events.h5')
new_df = pd.read_hdf('/eos/home-m/mlphd/temp2/test_several_channels_iteration_0_events.h5')#' + str(start_event) + '

#print ('new_df')
#print(new_df)
print('Merging')
for y in tqdm(range(start_event, total_events)):  #first signal
    #next_df = pd.read_hdf('/nfs/neutron-ml/temp/test_ch' + str(sel_channel) + '_iteration_' + str(y) +'_events.h5')
    next_df = pd.read_hdf('/eos/home-m/mlphd/temp2/test_several_channels_iteration_' + str(y) +'_events.h5')
    
    #print ('next_df')
    #print(next_df)
    new_df = pd.concat([new_df, next_df], ignore_index=True)
    #print ('new_df2')
    #print(new_df)
#%%purchase
#pd.to_numeric(new_df)

new_df['BoardId'] = new_df['BoardId'].astype('int')

new_df['ChannelId1'] = new_df['ChannelId1'].astype('int')
new_df['ChannelId2'] = new_df['ChannelId2'].astype('int')
new_df['TimeStamp1'] = new_df['TimeStamp1'].astype('int')
new_df['TimeStamp2'] = new_df['TimeStamp2'].astype('int')
new_df['Delay'] = new_df['Delay'].astype('int')
#new_df['Trace1'] = new_df['Trace1'].astype('int')


new_df.dtypes
#new_df = new_df.apply(pd.to_numeric, errors='ignore')

new_df.info()

final_events  = new_df.size
#%%
#new_df = new_df.convert_objects(convert_numeric=True)
#path = '/nfs/neutron-ml/test_ch' + str(sel_channel) + '_merged_' + str(final_events) + 'events_12delays.pickle'
path = '/eos/home-m/mlphd/test_merged_to_train_events_6to39delays_noNUMEXO_g_g_2nd_dataset_EF.pickle'

print("creating pickle file of channel")
new = new_df.to_pickle(path)
#
#path = '/nfs/neutron-ml/test_ch0_merged_events.ftr'
#feather.write_feather(new_df, path, compression='uncompressed')
#new_df = feather.read_feather(path)


#print (new_df)
#%%


read_df = pd.read_pickle(path)

print('reading pickle')
read_df.info()
#%%


#dirPath = '/nfs/neutron-ml/temp'
#dirPath = '/eos/home-m/mlphd/temp'

#try:
#    shutil.rmtree(dirPath)
#except OSError as e:
#    print(f"Error:{ e.strerror}")

#new_df.to_hdf('/nfs/neutron-ml/test_ch0_merged_events.h5', key='new_df', mode='w')  #function to convert and save in .h5
#reread = pd.read_hdf('/nfs/neutron-ml/ch0_Xevents.h5')
print('done')

print('DONE all channels')
    #total_artificial_events = len(reread.index)

    #print  ("total_artificial_events in ch0_Xevents.h5")

    #print (total_artificial_events)


    #print (df_final)
    #type(df_final)

    #hf = h5py.File('/nfs/neutron-ml/test.h5', 'w')

    #with h5py.File('/nfs/neutron-ml/test.h5', 'w') as f:
    #    dset = f.create_dataset("default", data = df_2_signals)



    #%%


# %%
