# %%
from re import T
import ROOT
#from os import read
import os
import errno
import sys
import tables  #convertion hdf
from numpy.core.fromnumeric import trace
print(sys.version)
import csv
import pandas as pd
import matplotlib.pyplot as plt
import uproot
import awkward as ak
#from root_pandas  import read_root
import numpy as np
import h5py
import shutil
import pickle
from tqdm import tqdm
from scipy.signal import find_peaks
#for i in tqdm(range(10)):
#with open('gamma_signal.csv') as File:  
#    reader = csv.reader(File)
#    for row in reader:
#        print(row)  nfs/neutron-ml /nfs/neutron-ml/event.root

# %%
#path = '/eos/home-m/mlphd/results_PSA_comparison_ch0_noNUMEXO_1stsignal_A.pickle'
#new_df = pd.read_pickle(path)

#path = '/eos/home-m/mlphd/results_PSA_comparison_ch0_noNUMEXO_1stsignal_B.pickle'
#next_df = pd.read_pickle(path)

#read_df = pd.concat([new_df, next_df], ignore_index=True)


#path = '/eos/home-m/mlphd/results_PSA_comparison_E2_ng_model.pickle'
path = '/eos/home-m/mlphd/results_PSA_1st_signal_comparison_ng_model_n_g_2nd_dataset_EF.pickle'
read_df = pd.read_pickle(path)
#read_df_all = read_df

read_df_all1 = read_df
distance = 0
print(read_df)
#%%
for y in tqdm(range(0, len(read_df))):
    
    trace_input = read_df["TraceFinal"][y]
    #trace_input = trace_input.tolist()
    #trace_input = trace_input[0]
    
    peaks = find_peaks(trace_input[45:60], height= 100, distance = 2 ,prominence=50)
    peak_pos = peaks[0]
    #plt.plot(trace_input)

    if np.any(peak_pos):
        height = peaks[1]['peak_heights']
        base_left = peaks[1]['left_bases']
        distance = peak_pos[0] - base_left[0]
        #print(y)
        #print(distance)
        #print(distance)
        if (distance) < 3 :#datssa = data.drop(labels=0, axis=0)
            #print("deleted")
            read_df_all1=read_df_all1.drop(labels=y, axis=0)
            #plt.plot(trace_input)
            #print(peaks)
       
print(read_df_all1)
read_df_all1 = read_df_all1.reset_index(drop=True)
print(read_df_all1)
read_df_all =read_df_all1
#read_df_all = read_df.loc[read_df.loc[:, 'Delay'] < 5]
# %%
#print(read_df)
'''
from turtle import color


fig2 = plt.figure(figsize=(10,6), dpi=600)      
plt.xlabel("samples")
plt.ylabel("value")
plt.hist(read_df['Slow/Fast_result'], bins = 600)

#plt.plot(np.array(range(232)),trace1[eventNumber].reshape(232))
#plt.plot(np.array(range(232)),trace2[eventNumber].reshape(232))

plt.xlim(0,0.5)

plt.legend(['Slow/Fast_result'])
plt.show()


fig2 = plt.figure(figsize=(10,6), dpi=600)      
plt.xlabel("samples")
plt.ylabel("value")
plt.hist(read_df['Slow/Fast_orig'], bins = 300, color= 'r')

#plt.plot(np.array(range(232)),trace1[eventNumber].reshape(232))
#plt.plot(np.array(range(232)),trace2[eventNumber].reshape(232))

#plt.xlim(0,0.5)

plt.legend(['Slow/Fast_orig'])
plt.show()'''



read_df_orig = read_df_all.loc[read_df_all.loc[:, 'Slow/Fast_orig'] > 0.6]
read_df_res = read_df_all.loc[read_df_all.loc[:, 'Slow/Fast_result'] > 0.45]

print("Count orig neutrons = " + str(len(read_df_orig)))
print("Count result neutrons = " + str(len(read_df_res)))

df_equal = read_df_all.loc[(read_df_all.loc[:, 'Slow/Fast_orig'] > 0.6) & (read_df_all.loc[:, 'Slow/Fast_result'] > 0.45)]
print("Count equal neutrons = " + str(len(df_equal)))


df_dif = read_df_all.loc[(read_df_all.loc[:, 'Slow/Fast_orig'] > 0.6) & (read_df_all.loc[:, 'Slow/Fast_result'] < 0.45)]
print("Count dif neutrons = " + str(len(df_dif)))

#%%

from sklearn.metrics import mean_squared_error, mean_absolute_error
y_true = read_df_all['Slow/Fast_orig']
#y_pred = read_df['Slow/Fast_result']

#indice = y_true.index[np.isinf(y_true)]
#read_df_all_true = read_df_all.drop(indice)
#y_true = read_df_all_true['Slow/Fast_orig']

print(y_true)

y_pred = read_df_all['Slow/Fast_result']
#indice = y_pred.index[np.isinf(y_pred)]
#read_df_all_pred = read_df_all.drop(indice)
#y_pred = read_df_all_pred['Slow/Fast_result']

print(y_pred)


#%%
mse = mean_squared_error(y_true, y_pred)
#mae = mean_absolute_error(y_true, y_pred)
#print ("Mean absolute error: " + str(mae))
print ("TOTAL Mean squared error: " + str(mse))
#%%
i =6
for i in range(6, 40):
    new_df = read_df.loc[read_df.loc[:, 'Delay'] == i]
    y_true = new_df['Slow/Fast_orig']
    y_pred = new_df['Slow/Fast_result']
    mse = round(mean_squared_error(y_true, y_pred),4)
    mae = round(mean_absolute_error(y_true, y_pred),4)
    print ("DELAY:"+ str(i))
    print ("Mean squared error: " + str(mse) + "\n")
 
#print(read_df)



#%%
fig = plt.figure(figsize=(10,5), dpi=600) 
plt.hist(x=y_true, bins=10000)
plt.title('Results PSA original signals')
plt.yscale('log')
plt.xlabel('Slow/Fast')
plt.ylabel('Frequency')
#plt.xlim(0,1)
plt.show()
#%%
fig = plt.figure(figsize=(10,5), dpi=600) 
plt.hist(x=y_pred, bins=1000)
plt.title('Results PSA reconstructed signals')
plt.yscale('log')
plt.xlabel('Slow/Fast')
plt.ylabel('Frequency')
plt.xlim(0,1)
plt.show()


#%%


#fig = plt.figure(figsize=(10,5), dpi=600)      
#plt.xlabel("samples")
#plt.ylabel("value")
#plt.ylim(8000,15000)
read_df_res = read_df_res.loc[read_df_res.loc[:, 'Delay'] < 10]
read_df_res = read_df_res.reset_index(drop=True)

#df_recons = df_recons.loc[df_recons.loc[:, 'Delay'] < 10]
#df_recons = df_recons.reset_index(drop=True)

for i in range(10,20):


    current_trace = read_df_res["Trace_orig"][i]
    recon_trace = read_df_res["Trace_result"][i]
    trace_input = read_df_res["TraceFinal"][i]
    #trace_input = trace_input.tolist()
    #trace_input = trace_input[0]

    #peaks = find_peaks(trace_input[0:60], height= 100, distance = 2,prominence=50)
    #height = peaks[1]['peak_heights']
    #base_left = peaks[1]['left_bases']
    #peak_pos = peaks[0]

    #if (peak_pos - base_left) > 2 :
        #print("Delay = " + str(read_df_res["Delay"][i]))

        #base_line = round(np.mean(current_trace[0:26]))
        #final_base =  base_line - base_line_origin
    #current_trace = current_trace - final_base
    #current_trace = np.roll(current_trace, -6)

    fig = plt.figure(figsize=(10,5), dpi=600)
    ax = fig.add_subplot()
    plt.plot(np.array(range(232)),current_trace.reshape(232), linestyle='-')#, s=1)
    plt.plot(np.array(range(232)),recon_trace.reshape(232), linestyle='-')#, s=1)
    plt.plot(trace_input, linestyle=':')#, s=1)
    ax.legend(['Original', 'Recconstructed','Pile-up signal'])
    ax.text(0.95, 0.2, "Delay = " + str(read_df_res["Delay"][i]) + "\n" + "Slow/Fast original = " + str(round(read_df_res["Slow/Fast_orig"][i], 4)) + "\n" + "Slow/Fast_reconstructed = " + str(round(read_df_res["Slow/Fast_result"][i], 4)),
        verticalalignment='bottom', horizontalalignment='right',
        transform=ax.transAxes,
        color='black', fontsize=10)
    
    #plt.xlim(45,55)
    #plt.ylim(14150,14400)
    plt.show()



#%%
read_df_res = read_df_res.loc[read_df_res.loc[:, 'Delay'] > 11]
read_df_res = read_df_res.reset_index(drop=True)


for i in range(40,50):
    current_trace = read_df_res["Trace_orig"][i]
    recon_trace = read_df_res["Trace_result"][i]
    print("Delay = " + str(read_df_res["Delay"][i]))

    plt.plot(np.array(range(232)),current_trace.reshape(232), linestyle='-')#, s=1)
    plt.plot(np.array(range(232)),recon_trace.reshape(232), linestyle=':')#, s=1)
    
    plt.show()

#%%
## Different result
df_dif = df_dif.reset_index(drop=True)


for i in range(10,30):
    current_trace = df_dif["Trace_orig"][i]
    recon_trace = df_dif["Trace_result"][i]
    print("Delay = " + str(df_dif["Delay"][i]))
    #base_line = round(np.mean(current_trace[0:26]))
    #final_base =  base_line - base_line_origin
    #current_trace = current_trace - final_base
    #current_trace = np.roll(current_trace, -6)




    plt.plot(np.array(range(232)),current_trace.reshape(232), linestyle='-')#, s=1)
    plt.plot(np.array(range(232)),recon_trace.reshape(232), linestyle=':')#, s=1)
    print("Slow/Fast_orig " + str(df_dif["Slow/Fast_orig"][i])+ "\n" + "Slow/Fast_result " + str(df_dif["Slow/Fast_result"][i]))

    plt.show()
#new_trace2 = trace2[eventNumber].reshape(232)
#plt.plot(np.array(range(232)),trace2[eventNumber].reshape(232))
#trace2_shift = np.roll(new_trace2, i+2+3)
#plt.plot(np.array(range(232)),trace2_shift)#, s=1)
# plt.show()
# fig.savefig('TraceFinal.png')

# fig = plt.figure()      
#plt.xlabel("samples")
#plt.ylabel("value")
#plt.ylim(8000,15000)
#current_trace = data.loc[eventNumber]['Trace']
#base_line = current_trace[0:26]
#current_trace = np.append(current_trace,base_line)
#current_trace = np.append(current_trace,base_line)
#current_trace = np.append(current_trace,base_line)
#current_trace = np.append(current_trace,base_line)
#current_trace = np.roll(current_trace, 10)
#current_trace = current_trace*-1
#current_trace = current_trace + data.loc[eventNumber]['Baseline']
#plt.plot(current_trace)#, s=1)
#plt.show()
#print (trace1[eventNumber])
# plt.show()
# fig.savefig('Trace1.png')


#plt.xlabel("samples")
#plt.ylabel("value")
#plt.ylim(10000,15000)
#plt.plot(np.array(range(232)),trace2[eventNumber].reshape(232))

# traceFinal[eventNumber].reshape(1,232,1))s3
'''
result, result2 = model.predict(current_trace.reshape(1,232,1))

#print (result)
#print(result2)
#print("result2")
#print(type(result2))
res2 = result2[0]
baseline2 = round(np.mean(res2[0:20]))
result2_inv = (res2 - baseline2)*-1

peaks = find_peaks(result2_inv, height= 500, distance = 2)
print(peaks)
#print (result2)   
# fig = plt.figure()      
plt.xlabel("samples")
plt.ylabel("value")
#plt.ylim(8000,15000)
plt.plot(np.array(range(232)),result.reshape(232), linestyle='--')#, s=1)
plt.plot(np.array(range(232)),result2.reshape(232), linestyle='--')
#plt.plot(result)
#plt.plot(result2)
plt.suptitle("Event Number " + str(eventNumber))

plt.legend(['Input','Prediction 1st signal','Prediction 2nd signal'])
plt.title("Channel = " + str(data.loc[i]["ChannelId1"]) + "    Timestamp = " + str(data.loc[i]["TimeStamp1"]))
plt.show()
#fig.savefig('real_pile-up_rec_no_NUMEXO/Event_' + str(eventNumber) + '_all_signals.png')
#fig.savefig('.png')


fig1 = plt.figure(figsize=(10,6))
plt.plot(np.array(range(232)),result.reshape(232), linestyle='--')#, s=1)
plt.xlabel("samples")
plt.ylabel("value")
plt.legend(['First reconstructed signal'])
plt.show()      
#fig1.savefig('real_pile-up_rec_no_NUMEXO/Event_' + str(eventNumber) + '_FIRST_signal.png')

fig1 = plt.figure(figsize=(10,6))
plt.plot(np.array(range(232)),result2.reshape(232), linestyle='--')#, s=1)
plt.xlabel("samples")
plt.ylabel("value")
plt.legend(['Second reconstructed signal'])
plt.show() 
#fig1.savefig('real_pile-up_rec_no_NUMEXO/Event_' + str(eventNumber) + '_SECOND_signal.png')


fig2 = plt.figure(figsize=(10,6))
baseline = round (np.mean(result[:20]))
trace_rec = (result - baseline) + (result2 - baseline) + baseline
plt.plot(np.array(range(232)),current_trace.reshape(232))#, s=1)
plt.plot(np.array(range(232)),trace_rec.reshape(232), linestyle='--')

plt.xlabel("samples")
plt.ylabel("value")
plt.legend(['Input signal','Combining 2 results'])
plt.suptitle("Event Number " + str(eventNumber))
plt.show()      

'''
# %%
