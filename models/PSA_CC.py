#%%
from __future__ import absolute_import, division, print_function
from inspect import trace

import pathlib

import os
from trace import Trace

# manually specify the GPUs to use
#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"]="0"

import matplotlib as m
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle as pk
import tables
import datetime

import time

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers import Dense, Activation, Conv1D
import tensorflow.keras.backend as K
from tensorflow.keras.callbacks import TensorBoard, LearningRateScheduler, ReduceLROnPlateau, CSVLogger, ModelCheckpoint, EarlyStopping
print(tf.__version__)

from scipy.io import readsav
from scipy.signal import deconvolve
from scipy.ndimage.interpolation import shift



import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)

from matplotlib import colors
import matplotlib
# matplotlib.use('Agg') #comment for jupiter plotting

#TURBO CHARGE YOUR PYTHON -- Make it parallel
import multiprocessing




#%%
#data = pd.read_hdf('/nfs/neutron-ml/test2.h5')
#data = pd.read_pickle('/nfs/neutron-ml/test_ch0_merged_3M_events.pickle')
#data = pd.read_pickle('/nfs/neutron-ml/test_ch0_merged_2206280events_12delays.pickle')
#data = pd.read_pickle('/nfs/neutron-ml/test_ch1_merged_3472224events_12delays.pickle')
#data = pd.read_pickle('/nfs/neutron-ml/test_ch2_merged_7741336events_12delays.pickle')
#data = pd.read_pickle('/nfs/neutron-ml/test_ch3_merged_9528632events_12delays.pickle')
#data = pd.read_pickle('/nfs/neutron-ml/test_ch4_merged_19521616events_12delays.pickle')
#data = pd.read_pickle('/nfs/neutron-ml/test_ch5_merged_2052848events_12delays.pickle')
#data = pd.read_pickle('/nfs/neutron-ml/test_ch6_merged_10813888events_12delays.pickle')
#data = pd.read_pickle('/nfs/neutron-ml/test_ch7_merged_16863496events_12delays.pickle')
data = pd.read_pickle('/nfs/neutron-ml/events_no_pileup.pickle')


# %%
