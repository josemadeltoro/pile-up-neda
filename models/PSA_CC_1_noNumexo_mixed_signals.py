# %%
from re import T
from unittest import result
import ROOT
#from os import read
import os
import errno
import sys
import tables  #convertion hdf
from numpy.core.fromnumeric import trace
print(sys.version)
import csv
import pandas as pd
import matplotlib.pyplot as plt
import uproot
import awkward as ak
#from root_pandas  import read_root
import numpy as np
import h5py
import shutil
import pickle
from tqdm import tqdm
from scipy.signal import find_peaks
#for i in tqdm(range(10)):
#with open('gamma_signal.csv') as File:  
#    reader = csv.reader(File)
#    for row in reader:
#        print(row)  nfs/neutron-ml /nfs/neutron-ml/event.root

#%%
print("Reading original data")
#path = '/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_E.pickle'
#path = '/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_E.pickle'

#df_no_pileup = pd.read_pickle(path)
#print(df_no_pileup)

#path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_A.pickle')
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_A_2nd_dataset_E.pickle')
df_origA = pd.read_pickle(path)
df_origA = df_origA.loc[df_origA.loc[:, 'Slow/Fast_orig'] < 0.6]
df_origA = df_origA.reset_index(drop=True)
print(df_origA)

#path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_B.pickle')
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_B_2nd_dataset_E.pickle')
df_origB = pd.read_pickle(path)
df_origB = df_origB.loc[df_origB.loc[:, 'Slow/Fast_orig'] < 0.6]
df_origB = df_origB.reset_index(drop=True)
print(df_origB)

#path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_C.pickle')
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_A_2nd_dataset_F.pickle')
df_origC = pd.read_pickle(path)
df_origC = df_origC.loc[df_origC.loc[:, 'Slow/Fast_orig'] < 0.6]
df_origC = df_origC.reset_index(drop=True)
print(df_origC)

#path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_D.pickle')
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_B_2nd_dataset_F.pickle')
df_origD = pd.read_pickle(path)
df_origD = df_origD.loc[df_origD.loc[:, 'Slow/Fast_orig'] < 0.6]
df_origD = df_origD.reset_index(drop=True)
print(df_origD)

df_no_pileup = pd.concat([df_origA, df_origB, df_origC, df_origD], ignore_index=1)



# %%
print("Reading results")
path2 = '/eos/home-m/mlphd/reconstruction_signals_noNUMEXO_ng_model_g_g_2nd_dataset_EF.pickle'

#path2 = '/eos/home-m/mlphd/results_noNUMEXO_E2_ng_model.pickle'
#channel = 0
#path2 = '/nfs/neutron-ml/ch9_5000_results.pickle'
df_results = pd.read_pickle(path2)
#print(df_results)

# %%
df_final = pd.DataFrame()
df_final = pd.DataFrame(columns=['BoardId', 'ChannelId', 'TimeStamp', 'CFD_PSA', 'Slow_PSA_orig','Slow_PSA_result','Fast_PSA_orig','Fast_PSA_result', 'Slow/Fast_orig', 'Slow/Fast_result', 'Delay','Trace_orig', 'Trace_result' , 'TraceFinal'])
print("Start analisys")

Nevents = int(len(df_results)/2)
#Nevents_end = Nevents*2

# %%
for i in tqdm(range(0, len(df_results))):#range(Nevents, len(df_results))
    df_single_result = df_results[i:i+1]
    df_single_result.reset_index()
    #print (df_single_result)
    ##r --------  removing baseline
    current_trace_orig = df_single_result.loc[:,'Trace1']
    current_trace_orig = current_trace_orig.tolist()
    current_trace_orig = current_trace_orig[0]
    delay = df_single_result.iloc[0]['Delay']
    trace_np_orig = np.array(current_trace_orig)*-1
    baseline_orig = round (np.mean(trace_np_orig[:19]))
    trace_NB = (trace_np_orig - baseline_orig)
    #trace_NB = np.roll(trace_NB, -6) #moving to put the peak in the same point

    current_trace_res = df_single_result.loc[:,'Trace1_result']

    current_trace_res = current_trace_res.tolist()
    current_trace_res = current_trace_res[0]
    trace_np_res = np.array(current_trace_res)*-1
    baseline_res = round (np.mean(trace_np_res[:19]))
    trace_res_NB = (trace_np_res - baseline_res)
    
    current_trace_final = df_single_result.loc[:,'TraceFinal']
    current_trace_final = current_trace_final.tolist()
    current_trace_final = current_trace_final[0]
    ##  -----------   matching signals
    #print(df_single_result)
    my_timestamp = df_no_pileup.loc[:, 'Timestamp'] == df_single_result.loc[i,'TimeStamp1']
    my_timestamp = df_no_pileup.loc[my_timestamp]
    my_timestamp.head()
    my_channel = my_timestamp.loc[:,'Channel'] == df_single_result.loc[i,'ChannelId1']
    event_matched = my_timestamp.loc[my_channel]
    event_matched.head()
    event_matched = event_matched.reset_index()
    #sprint(event_matched)
    #print(event_matched)

    ###  --------  Analisys Original Trace
    atp = 0 
    result_fast = 0
    result_slow = 0
    result_res_fast = 0
    result_res_slow = 0
    result_zc = 49#int(round(event_matched.loc[:,'CFD_PSA']))  ###value from original analisys
    start_init = -12 ###Value from config file -12 for channel 9 and 10
    length_fast = 4 ###Value from config file -12 for channel 9 and 10
    length_slow = 60 ###Value from config file -12 for channel 9(42)  /// channel 10 (60)
    stoppoint = result_zc + length_fast
    initpoint = result_zc + start_init
    initpoint_slow = result_zc + length_fast


    ###  --------  Analisys Result Trace

    if result_zc + length_fast < 232:
        stoppoint = result_zc + length_fast
    else:
        232

    for i in range(initpoint,stoppoint+1):
        atp = atp+1
        if ((i >= result_zc + 25) and (i <= result_zc + 35) ):
            continue
        if   not((atp == 0) or (atp == length_fast - 1)) :
            result_res_fast += trace_res_NB[i]
        else:
            result_res_fast += trace_res_NB[i]
    #print(result_res_fast)


    if result_zc + length_fast + length_slow < 232:
        stoppoint = result_zc + length_fast + length_slow
    else:
        232

    for i in range(initpoint_slow,stoppoint+1):

        if  not ((atp == 0) or (atp == length_slow - 1)) :
            result_res_slow += trace_res_NB[i]
        else:
            result_res_slow += trace_res_NB[i] #// This is "trap part" of Int_t.

    #print(result_res_slow)

    #value_psa_orig = result_slow/result_fast
    value_psa_result = result_res_slow/result_res_fast
    value_psa_orig = event_matched.loc[0,'Slow/Fast_orig']
    result_slow = event_matched.loc[0,'Slow_PSA_orig']
    result_fast = event_matched.loc[0,'Fast_PSA_orig']
    #print(value_psa_result)
    #print(value_psa_orig)
    #print(result_slow)
    #print(event_matched['Trace'])
    #plt.plot(event_matched['Trace'])
    #plt.plot(current_trace_res)
    #plt.show()
    df_final = df_final.append({'BoardId':event_matched.iloc[0]['Board'], 'ChannelId':event_matched.iloc[0]['Channel'], 'TimeStamp':event_matched.iloc[0]['Timestamp'], 'CFD_PSA':49, 'Slow_PSA_orig':result_slow,'Slow_PSA_result':result_res_slow,'Fast_PSA_orig':result_fast,'Fast_PSA_result':result_res_fast, 'Slow/Fast_orig':value_psa_orig, 'Slow/Fast_result':value_psa_result, 'Delay':delay,'Trace_orig':current_trace_orig, 'Trace_result':current_trace_res, 'TraceFinal':current_trace_final}, ignore_index=True)
    
    #print(df_final)

#%%
path = '/eos/home-m/mlphd/results_PSA_1st_signal_comparison_ng_model_g_g_2nd_dataset_EF.pickle'

df_final.to_pickle(path)

print("END")

    



# %%
