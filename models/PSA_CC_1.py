# %%
from re import T
import ROOT
#from os import read
import os
import errno
import sys
import tables  #convertion hdf
from numpy.core.fromnumeric import trace
print(sys.version)
import csv
import pandas as pd
import matplotlib.pyplot as plt
import uproot
import awkward as ak
#from root_pandas  import read_root
import numpy as np
import h5py
import shutil
import pickle
from tqdm import tqdm
from scipy.signal import find_peaks
#for i in tqdm(range(10)):
#with open('gamma_signal.csv') as File:  
#    reader = csv.reader(File)
#    for row in reader:
#        print(row)  nfs/neutron-ml /nfs/neutron-ml/event.root

# %%
print("Reading original data")
path = '/eos/home-m/mlphd/numexo43_merged_no_pile_up.pickle'
df_no_pileup = pd.read_pickle(path)
#print(df_no_pileup)

# %%
print("Reading results")
path2 = '/eos/home-m/mlphd/ch10_100000_resultsA.pickle'
channel = 10
#path2 = '/nfs/neutron-ml/ch9_5000_results.pickle'
df_results = pd.read_pickle(path2)
#print(df_results)

# %%
df_final = pd.DataFrame()
df_final = pd.DataFrame(columns=['BoardId', 'ChannelId', 'TimeStamp', 'CFD_PSA', 'Slow_PSA_orig','Slow_PSA_result','Fast_PSA_orig','Fast_PSA_result', 'Slow/Fast_orig', 'Slow/Fast_result', 'Delay','Trace_orig', 'Trace_result' ])
print("Start analisys")
# %%
for i in tqdm(range(0, 100000)):
    df_single_result = df_results[i:i+1]
    df_single_result.reset_index()
    #print (df_single_result)
    ##r --------  removing baseline
    current_trace_orig = df_single_result.loc[:,'Trace1']
    current_trace_orig = current_trace_orig.tolist()
    current_trace_orig = current_trace_orig[0]
    delay = df_single_result.iloc[0]['Delay']
    trace_np_orig = np.array(current_trace_orig)*-1
    baseline_orig = round (np.mean(trace_np_orig[:19]))
    trace_NB = (trace_np_orig - baseline_orig)

    current_trace_res = df_single_result.loc[:,'Trace1_result']

    current_trace_res = current_trace_res.tolist()
    current_trace_res = current_trace_res[0]
    trace_np_res = np.array(current_trace_res)*-1
    baseline_res = round (np.mean(trace_np_res[:19]))
    trace_res_NB = (trace_np_res - baseline_res)
    
    ##  -----------   matching signals
    #print(df_single_result)
    my_timestamp = df_no_pileup.loc[:, 'TimeStamp'] == df_single_result.loc[i,'TimeStamp1']
    my_timestamp = df_no_pileup.loc[my_timestamp]
    my_timestamp.head()
    my_channel = my_timestamp.loc[:,'ChannelId'] == channel
    event_matched = my_timestamp.loc[my_channel]
    event_matched.head()
    event_matched.reset_index()

    ###  --------  Analisys Original Trace
    atp = 0 
    result_fast = 0
    result_slow = 0
    result_res_fast = 0
    result_res_slow = 0
    result_zc = int(round(event_matched.loc[:,'CFD_PSA']))  ###value from original analisys
    start_init = -12 ###Value from config file -12 for channel 9 and 10
    length_fast = 4 ###Value from config file -12 for channel 9 and 10
    length_slow = 60 ###Value from config file -12 for channel 9(42)  /// channel 10 (60)
    stoppoint = result_zc + length_fast
    initpoint = result_zc + start_init
    initpoint_slow = result_zc + length_fast
    
    ###   ---------------------------   Integral Fast

    for i in range(initpoint,stoppoint+1):
        atp = atp+1
        if ((i >= result_zc + 25) and (i <= result_zc + 35) ):
            continue
        if   not((atp == 0) or (atp == length_fast - 1)) :
            result_fast += trace_NB[i]
        else:
            result_fast +=  trace_NB[i]

    #print (result_fast)
    ###   ---------------------------   Integral Slow
    if result_zc + length_fast + length_slow < 232:
        stoppoint = result_zc + length_fast + length_slow
    else:
        232
    for i in range(initpoint_slow,stoppoint+1):

        if  not ((atp == 0) or (atp == length_slow - 1)) :
            result_slow += trace_NB[i]
        else:
            result_slow += trace_NB[i] #// This is "trap part" of Int_t.

    #print(result_slow)


    ###  --------  Analisys Result Trace

    if result_zc + length_fast < 232:
        stoppoint = result_zc + length_fast
    else:
        232

    for i in range(initpoint,stoppoint+1):
        atp = atp+1
        if ((i >= result_zc + 25) and (i <= result_zc + 35) ):
            continue
        if   not((atp == 0) or (atp == length_fast - 1)) :
            result_res_fast += trace_res_NB[i]
        else:
            result_res_fast += trace_res_NB[i]
    #print(result_res_fast)


    if result_zc + length_fast + length_slow < 232:
        stoppoint = result_zc + length_fast + length_slow
    else:
        232

    for i in range(initpoint_slow,stoppoint+1):

        if  not ((atp == 0) or (atp == length_slow - 1)) :
            result_res_slow += trace_res_NB[i]
        else:
            result_res_slow += trace_res_NB[i] #// This is "trap part" of Int_t.

    #print(result_res_slow)

    value_psa_orig = result_slow/result_fast
    value_psa_result = result_res_slow/result_res_fast


    df_final = df_final.append({'BoardId':event_matched.iloc[0]['BoardId'], 'ChannelId':event_matched.iloc[0]['ChannelId'], 'TimeStamp':event_matched.iloc[0]['TimeStamp'], 'CFD_PSA':event_matched.iloc[0]['CFD_PSA'], 'Slow_PSA_orig':result_slow,'Slow_PSA_result':result_res_slow,'Fast_PSA_orig':result_fast,'Fast_PSA_result':result_res_fast, 'Slow/Fast_orig':value_psa_orig, 'Slow/Fast_result':value_psa_result, 'Delay':delay,'Trace_orig':current_trace_orig, 'Trace_result':current_trace_res}, ignore_index=True)
    
    #print(df_final)


path = '/eos/home-m/mlphd/results_PSA_comparison_ch10_100000events_1stsignal.pickle'

df_final.to_pickle(path)

print("END")

    



# %%
