# %%
from re import T
import ROOT
#from os import read
import os
import errno
import sys
import tables  #convertion hdf
from numpy.core.fromnumeric import trace
print(sys.version)
import csv
import pandas as pd
import matplotlib.pyplot as plt
import uproot
import awkward as ak
#from root_pandas  import read_root
import numpy as np
import h5py
import shutil
import pickle
from tqdm import tqdm
from scipy.signal import find_peaks
#for i in tqdm(range(10)):
#with open('gamma_signal.csv') as File:  
#    reader = csv.reader(File)
#    for row in reader:
#        print(row)  nfs/neutron-ml /nfs/neutron-ml/event.root

# %%
print("Reading original data")
path = '/eos/home-m/mlphd/data_only_NO_pile-up_noNUMEXO_2nd_dataset_H.pickle'
df_no_pileup = pd.read_pickle(path)
#print(df_no_pileup)
print(len(df_no_pileup))

# %%
traceFinal=np.array([x for x in df_no_pileup['Trace']]).reshape(-1,232,1).astype('float32')
df_final = pd.DataFrame()
df_final = pd.DataFrame(columns=['Board', 'Channel', 'Timestamp', 'Slow_PSA_orig','Fast_PSA_orig', 'Slow/Fast_orig', 'Trace'])

print("Start analisys")
init_event = int(len(df_no_pileup)/2)#0#
final_event = len(df_no_pileup)#int(len(df_no_pileup)/2)#len(df_no_pileup)#
print(final_event)
# %%
for i in tqdm(range(init_event, final_event)):
    #eventNumber = i
    #current_trace_orig = df_no_pileup.loc[:,'Trace']
    #print(current_trace_orig)
    #current_trace_orig = traceFinal[eventNumber]
    #current_trace_orig = current_trace_orig.tolist()
    #print(current_trace_orig)
    #current_trace_orig = current_trace_orig[0]
    #print(current_trace_orig)
    #current_trace_orig = np.roll(current_trace_orig, -6)
    #print(current_trace_orig)
    #df_single_result = df_results[i:i+1]
    #df_single_result.reset_index()
    #print (df_single_result)
    ##r --------  removing baseline
    current_trace_orig = df_no_pileup.loc[:,'Trace']
    current_trace_orig = current_trace_orig[i]
    
    current_channel = df_no_pileup.loc[i,'Channel']
    #print(current_channel)
   # current_channel = current_channel[i]

    current_time = df_no_pileup.loc[i,'Timestamp']
    #print(current_time)
    #current_time = current_time[i]
    #current_trace_orig = current_trace_orig.tolist()
    #current_trace_orig = np.roll(current_trace_orig, -5)
    #current_trace_orig = current_trace_orig[0]
    #print(current_trace_orig)
    #delay = df_single_result.iloc[0]['Delay']

    trace_np_orig = np.array(current_trace_orig)*-1

    baseline_orig = round (np.mean(trace_np_orig[:19]))

    trace_NB = (trace_np_orig - baseline_orig)

    peaks = find_peaks(trace_NB, height= 200, distance = 2)
    delay = peaks[0]-49 
    #print(len(peaks[0]))
    if len(peaks[0]) < 2:
        trace_NB = np.roll(trace_NB, -delay[0])
        current_trace_orig= np.roll(current_trace_orig, -delay[0])
        #print(peaks[0])
        #print(delay[0])
        #print(trace_NB)

        #plt.plot(current_trace_orig)
        #plt.plot(trace_NB)


        ###  --------  Analisys Original Trace
        atp = 0 
        result_fast = 0
        result_slow = 0
        result_res_fast = 0
        result_res_slow = 0
        result_zc = 49 #int(round(event_matched.loc[:,'CFD_PSA']))  ###value from original analisys
        start_init = -12 ###Value from config file -12 for channel 9 and 10
        length_fast = 4 ###Value from config file -12 for channel 9 and 10
        length_slow = 60 ###Value from config file -12 for channel 9(42)  /// channel 10 (60)
        stoppoint = result_zc + length_fast
        initpoint = result_zc + start_init
        initpoint_slow = result_zc + length_fast
        
        ###   ---------------------------   Integral Fast

        for i in range(initpoint,stoppoint+1):
            atp = atp+1
            if ((i >= result_zc + 25) and (i <= result_zc + 35) ):
                continue
            if   not((atp == 0) or (atp == length_fast - 1)) :
                result_fast += trace_NB[i]
            else:
                result_fast +=  trace_NB[i]

        #print (result_fast)
        ###   ---------------------------   Integral Slow
        if result_zc + length_fast + length_slow < 232:
            stoppoint = result_zc + length_fast + length_slow
        else:
            232
        for i in range(initpoint_slow,stoppoint+1):

            if  not ((atp == 0) or (atp == length_slow - 1)) :
                result_slow += trace_NB[i]
            else:
                result_slow += trace_NB[i] #// This is "trap part" of Int_t.

        value_psa_orig = result_slow/result_fast
        #print(result_slow)

        #value_psa_result = result_res_slow/result_res_fast


        df_final = df_final.append({'Board':0, 'Channel':current_channel, 'Timestamp':current_time, 'Slow_PSA_orig':result_slow,'Fast_PSA_orig':result_fast, 'Slow/Fast_orig':value_psa_orig, 'Trace':current_trace_orig}, ignore_index=True)
        
    #print(df_final)
#%%
print(df_final)

#plt.hist(df_final.loc[:,'Slow/Fast_orig'])

#%%
path = '/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_B_2nd_dataset_H.pickle'
print('saving')
df_final.to_pickle(path)

print("END")

    



# %%
