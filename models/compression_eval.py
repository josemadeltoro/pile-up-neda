#%%
import pandas as pd
import matplotlib.pyplot as plt
import uproot
df_final = pd.DataFrame()

#df_final['BoardId'] = None
#df_final['ChannelId'] = None
#df_final['TimeStamp1'] = None
#df_final['TimeStamp2'] = None
#df_final['Trace1'] = None
#df_final['Trace2'] = None
#df_final['TraceFinal'] = None
#df_final['Delay'] = None


#print("df_final")
#print(df_final)
#print(df_final.dtypes)

#with uproot.open("/nfs/neutron-ml/event.root") as file:
filename = "/nfs/neutron-ml/event.root"
file = uproot.open(filename)
print(file.keys())
tree = file["merged_tree"]
#print (tree)
#tree.keys()
#print(tree.keys())
#tree['BoardId'].array()
df_original = tree.arrays(['BoardId', 'ChannelId', 'TimeStamp', 'CFD_PSA', 'CFD_RAW', 'Slow_PSA', 'Slow_RAW', 'Fast_PSA', 'Fast_RAW', 'TOF_PSA', 'TOF_RAW', 'NbZC', 'Baseline','Trace'], library="pd")
#print (df_original)
df_orig1 = df_original[['BoardId', 'ChannelId', 'TimeStamp', 'CFD_PSA', 'CFD_RAW', 'Slow_PSA', 'Slow_RAW', 'Fast_PSA', 'Fast_RAW', 'TOF_PSA', 'TOF_RAW', 'NbZC', 'Baseline']]
#print (df_orig1)

df_original.drop(['BoardId', 'ChannelId', 'TimeStamp', 'CFD_PSA', 'CFD_RAW', 'Slow_PSA', 'Slow_RAW', 'Fast_PSA', 'Fast_RAW', 'TOF_PSA', 'TOF_RAW', 'NbZC', 'Baseline'], inplace=True, axis=1)

#print (df_original)
df_original['Trace'] = df_original.values.tolist()

df_orig = pd.concat([df_orig1, df_original['Trace']], axis=1)

#print (df_original.iloc[[0:10]df_original.loc['Trace']])

#df_traces = tree.arrays(['Trace'], library="pd")

#df_traces['Trace'] = df_traces.values.tolist()
#df_orig['Trace'] = df_orig[['Trace[*]']].values.tolist()
#df_orig = pd.DataFrame(df_orig)
#df_orig = ak.to_pandas(df_orig)


#tree.pd.df(['BoardId', 'ChannelId', 'TimeStamp', 'CFD_PSA', 'CFD_RAW', 'Slow_PSA', 'Slow_RAW', 'Fast_PSA', 'Fast_RAW', 'TOF_PSA', 'TOF_RAW', 'NbZC', 'Baseline', 'Trace'])
#merged_tree.arrays(["px1", "py1", "pz1"], library="pd")
#tree.pandas.df()
 #   tree.pandas.df(["*"])

#df_uproot = df_orig.pandas.df("merged_tree")
#print(df_orig)
#df_orig = read_root(file, "merged_tree") # Pandas convertion
print (type(df_orig))
print (type(df_final))
print (df_orig)

# %%
df_no_pileup =df_orig.loc[df_orig.loc[:, 'NbZC'] == 0] # Taking no pile-up events

df_init = df_no_pileup.loc[:, ['BoardId', 'ChannelId', 'TimeStamp', 'Trace']] # Keeping the important parameters


#for to change channel

sel_channel = 8

df_single_channel =df_init.loc[df_init.loc[:, 'ChannelId'] == sel_channel] # Taking single channel

df_single_channel = df_single_channel.reset_index(drop=True)

#only_traces = df_original.loc[0:10,['Trace']]
#%%
import numpy as np
only_traces = df_single_channel.loc[0:5000,['Trace']]
#only_traces = df_single_channel.loc[0:10,['Trace']]

df_lens = pd.DataFrame()
#lens = [0]*232
lens = np.zeros(232, dtype=np.int64)
df_lens['Trace'] = [lens]
#print (len(df_lens['Trace']))
#lens = [0]*232
samples = list(range(0,232))
trace = list(range(len(only_traces)))
df_lens_all = pd.DataFrame(index= trace,columns=samples)
print (df_lens_all)
#%%

#df_lens_all['Trace'] = []
#print (df_lens_all)
#trace = [14861, 14866, 14865, 14864, 14865, 14864, 14861, 14864, 14862, 14860, 14863, 14864, 14863, 14867, 14862, 14867, 14864, 14866, 14868, 14863, 14860, 14865, 14862, 14867, 14863, 14861, 14865, 14863, 14863, 14863, 14865, 14861, 14864, 14865, 14864, 14863, 14865, 14859, 14866, 14866, 14865, 14864, 14862, 14866, 14860, 14865, 14755, 10764, 9006, 11471, 13231, 13909, 14512, 14460, 14421, 14538, 14654, 14725, 14765, 14728, 14665, 14696, 14787, 14792, 14707, 14801, 14820, 14819, 14826, 14825, 14801, 14818, 14847, 14852, 14831, 14817, 14827, 14897, 14797, 14737, 14797, 14814, 14832, 14858, 14855, 14853, 14804, 14850, 14858, 14843, 14861, 14859, 14855, 14861, 14860, 14859, 14835, 14856, 14864, 14863, 14859, 14830, 14856, 14856, 14861, 14862, 14858, 14860, 14859, 14856, 14858, 14865, 14861, 14859, 14864, 14863, 14860, 14859, 14850, 14857, 14849, 14853, 14858, 14860, 14865, 14865, 14865, 14813, 14851, 14858, 14859, 14858, 14817, 14854, 14862, 14859, 14861, 14861, 14862, 14861, 14863, 14862, 14858, 14863, 14814, 14852, 14863, 14859, 14864, 14862, 14861, 14860, 14861, 14858, 14861, 14863, 14862, 14865, 14863, 14861, 14862, 14861, 14865, 14864, 14863, 14866, 14864, 14862, 14863, 14862, 14864, 14859, 14863, 14861, 14865, 14863, 14863, 14863, 14861, 14862, 14861, 14863, 14862, 14864, 14861, 14862, 14863, 14863, 14861, 14860, 14863, 14864, 14850, 14856, 14841, 14856, 14864, 14867, 14860, 14865, 14862, 14864, 14862, 14868, 14863, 14859, 14859, 14862, 14864, 14835, 14858, 14863, 14862, 14865, 14861, 14862, 14865, 14867, 14864, 14862, 14858, 14861, 14861, 14860, 14860, 14823, 14863, 14862, 14863, 14869, 14863, 14869]
import numpy as np
trace_dif = [0]*232

all_lens = [16]
fig = plt.figure(figsize=(10,6), dpi=600)      
plt.xlabel("Samples in differences")
plt.ylabel("Bits")

for j in range(len(only_traces.index)):
    trace = only_traces.iloc[j].tolist()## coversion to list one trace
    trace = trace[0] #taking the trace

    lens = np.zeros(232, dtype=np.int64)

    for i in range(len(trace)):

        if i == 0:
            trace_dif[i] = trace[i]
            
            lens[i] = 16
            df_lens_all[i][j] = 16
            #print(df_lens_all)
        else:
            lens[i] = 8
            
            temp = trace[i]-trace[i - 1]
            trace_dif[i] = format(temp, "b")
            if int(trace_dif[i])>0:
                lens[i] = len(trace_dif[i])+1
                #print (df_lens_all)
                df_lens_all[i][j] = len(trace_dif[i])+1
                #print(df_lens_all)
            else:
                lens[i] = len(trace_dif[i])
                df_lens_all[i][j] = len(trace_dif[i])




'''fig = plt.figure(figsize=(12,8), dpi=800) 
plt.xlabel("Samples in differences", fontsize=18)
plt.ylabel("Bits", fontsize=16)
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
df = pd.read_csv ('compressed_traces.csv')
from matplotlib.colors import LogNorm
x = df.iloc[:,0]
y = df.iloc[:,1]
w = df.iloc[:,2]
h = plt.hist2d(x, y,density=True, weights = w, bins = (116,15), norm=LogNorm())

cbar=fig.colorbar(h[3])
cbar.ax.tick_params(labelsize=12)
plt.show()'''

'''    new_lens = lens
    #splits = np.array_split(listA, 3)

    df_lens['Trace'] = [new_lens]
    

    df_lens_all = df_lens_all.append(df_lens, ignore_index=True)'''

print(df_lens_all)

#plt.hist(df_lens_all, bins = 10, color = "blue", rwidth=0.9)

#print(df_lens)
#plt.show() 
# %%
