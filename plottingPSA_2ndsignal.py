# %%
from re import T
import ROOT
#from os import read
import os
import errno
import sys
import tables  #convertion hdf
from numpy.core.fromnumeric import trace
print(sys.version)
import csv
import pandas as pd
import matplotlib.pyplot as plt
import uproot
import awkward as ak
#from root_pandas  import read_root
import numpy as np
import h5py
import shutil
import pickle
from tqdm import tqdm
from scipy.signal import find_peaks
#for i in tqdm(range(10)):
#with open('gamma_signal.csv') as File:  
#    reader = csv.reader(File)
#    for row in reader:
#        print(row)  nfs/neutron-ml /nfs/neutron-ml/event.root

# %%
path = '/nfs/neutron-ml/results_PSA_comparison_ch10_500000events_2ndsignal.pickle'
read_df = pd.read_pickle(path)

# %%
y_true = read_df['Slow/Fast_orig']

# %%
indice = y_true.index[np.isinf(y_true)]

# %%
y_true = y_true.drop(indice)

# %%
read_df = read_df.drop(indice)

# %%
y_true = read_df['Slow/Fast_orig']

# %%
y_pred = read_df['Slow/Fast_result']

# %%
from sklearn.metrics import mean_squared_error, mean_absolute_error


# %%
mse = mean_squared_error(y_true, y_pred)


# %%
print ("TOTAL Mean squared error: " + str(mse))

# %%
i =3
for i in range(3, 12):
    new_df = read_df.loc[read_df.loc[:, 'Delay'] == i]
    y_true = new_df['Slow/Fast_orig']
    y_pred = new_df['Slow/Fast_result']
    mse = round(mean_squared_error(y_true, y_pred),4)
    mae = round(mean_absolute_error(y_true, y_pred),4)
    print ("DELAY:"+ str(i))
    print ("Mean squared error: " + str(mse) + "\n")



# %%
