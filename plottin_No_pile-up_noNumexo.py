# %%
from re import T
import ROOT
#from os import read
import os
import errno
import sys
import tables  #convertion hdf
from numpy.core.fromnumeric import trace
print(sys.version)
import csv
import pandas as pd
import matplotlib.pyplot as plt
import uproot
import awkward as ak
#from root_pandas  import read_root
import numpy as np
import h5py
import shutil
import pickle
from tqdm import tqdm
from scipy.signal import find_peaks
#for i in tqdm(range(10)):
#with open('gamma_signal.csv') as File:  
#    reader = csv.reader(File)
#    for row in reader:
#        print(row)  nfs/neutron-ml /nfs/neutron-ml/event.root

# %%
path = ('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_D.pickle')

# %%
#new_df = pd.read_pickle(path)
df = pd.read_pickle(path)
# %%
#y_true = new_df['Slow/Fast_orig']

# %%
#indice = y_true.index[np.isinf(y_true)]

# %%
#df = new_df.drop(indice)

# %%
print(df)

#df.to_pickle('/eos/home-m/mlphd/results_PSA_nopile-up_noNUMEXO_ALL.pickle')
# %%
plt.hist(x=df['Slow/Fast_result'], bins=10000)
plt.title('Results PSA result signals')
plt.yscale('log')
plt.xlim(0, 1)
plt.xlabel('Slow/Fast')
plt.ylabel('Frequency')
plt.show()



# %%
