#%%
from re import T
import ROOT
#from os import read
import os
import errno
import sys
import tables  #convertion hdf
from numpy.core.fromnumeric import trace
print(sys.version)
import csv
import pandas as pd
import matplotlib.pyplot as plt
import uproot
import awkward as ak
#from root_pandas  import read_root
import numpy as np
import h5py
import shutil
import pickle
from tqdm import tqdm
from scipy.signal import find_peaks
#for i in tqdm(range(10)):
#with open('gamma_signal.csv') as File:  
#    reader = csv.reader(File)
#    for row in reader:
#        print(row)  nfs/neutron-ml /nfs/neutron-ml/event.root
#%%
#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"]="0"

df_final = pd.DataFrame()

#df_final['BoardId'] = None
#df_final['ChannelId'] = None
#df_final['TimeStamp1'] = None
#df_final['TimeStamp2'] = None
#df_final['Trace1'] = None
#df_final['Trace2'] = None
#df_final['TraceFinal'] = None
#df_final['Delay'] = None


#print("df_final")
#print(df_final)
#print(df_final.dtypes)

#with uproot.open("/nfs/neutron-ml/event.root") as file:
filename = "/nfs/neutron-ml/event.root"
file = uproot.open(filename)
print(file.keys())
tree = file["merged_tree"]
#print (tree)
#tree.keys()
#print(tree.keys())
#tree['BoardId'].array()
df_original = tree.arrays(['BoardId', 'ChannelId', 'TimeStamp', 'CFD_PSA', 'CFD_RAW', 'Slow_PSA', 'Slow_RAW', 'Fast_PSA', 'Fast_RAW', 'TOF_PSA', 'TOF_RAW', 'NbZC', 'Baseline','Trace'], library="pd")
#print (df_original)
df_orig1 = df_original[['BoardId', 'ChannelId', 'TimeStamp', 'CFD_PSA', 'CFD_RAW', 'Slow_PSA', 'Slow_RAW', 'Fast_PSA', 'Fast_RAW', 'TOF_PSA', 'TOF_RAW', 'NbZC', 'Baseline']]
#print (df_orig1)

df_original.drop(['BoardId', 'ChannelId', 'TimeStamp', 'CFD_PSA', 'CFD_RAW', 'Slow_PSA', 'Slow_RAW', 'Fast_PSA', 'Fast_RAW', 'TOF_PSA', 'TOF_RAW', 'NbZC', 'Baseline'], inplace=True, axis=1)

#print (df_original)
df_original['Trace'] = df_original.values.tolist()

df_orig = pd.concat([df_orig1, df_original['Trace']], axis=1)



#df_traces = tree.arrays(['Trace'], library="pd")

#df_traces['Trace'] = df_traces.values.tolist()
#df_orig['Trace'] = df_orig[['Trace[*]']].values.tolist()
#df_orig = pd.DataFrame(df_orig)
#df_orig = ak.to_pandas(df_orig)


#tree.pd.df(['BoardId', 'ChannelId', 'TimeStamp', 'CFD_PSA', 'CFD_RAW', 'Slow_PSA', 'Slow_RAW', 'Fast_PSA', 'Fast_RAW', 'TOF_PSA', 'TOF_RAW', 'NbZC', 'Baseline', 'Trace'])
#merged_tree.arrays(["px1", "py1", "pz1"], library="pd")
#tree.pandas.df()
 #   tree.pandas.df(["*"])

#df_uproot = df_orig.pandas.df("merged_tree")
#print(df_orig)
#df_orig = read_root(file, "merged_tree") # Pandas convertion
print (type(df_orig))
print (type(df_final))
print (df_orig)
#print (df_traces)
#df_orig.show()

#%%
df_pileup =df_orig.loc[df_orig.loc[:, 'NbZC'] == 1]

df_init = df_pileup.loc[:, ['BoardId', 'ChannelId', 'TimeStamp', 'Trace']] # Keeping the important parameters


#for to change channel

sel_channel = 15

df_single_channel =df_init.loc[df_init.loc[:, 'ChannelId'] == sel_channel] # Taking single channel

df_single_channel = df_single_channel.reset_index(drop=True)

df_single_channel.info()

path = '/nfs/neutron-ml/test_ch' + str(sel_channel) + '_pileup_events.pickle'

new = df_single_channel.to_pickle(path)

read_df = pd.read_pickle(path)

#Trace = read_df.loc


print('reading pickle')
read_df.info()

#%%
new_df = pd.DataFrame(columns=['BoardId', 'ChannelId', 'TimeStamp', 'Trace'])
#print (read_df)
#print (read_df.loc[1])
new_df.info()
new_df = new_df.append([read_df.loc[6], read_df.loc[40], read_df.loc[49], read_df.loc[100], read_df.loc[147], read_df.loc[193],read_df.loc[210], read_df.loc[333], read_df.loc[585], read_df.loc[689], read_df.loc[770]], ignore_index=True)

print (new_df)
size =len(new_df)
print(size)
#read_df.loc[38]
path = '/nfs/neutron-ml/test_ch' + str(sel_channel) + '_' + str(size) + '_pileup_events.pickle'

new = new_df.to_pickle(path)




read_df = pd.read_pickle(path)

#%%
print(len(df_single_channel))

for i in range(0, 50):
    current_trace = np.array(df_single_channel.loc[i]['Trace'])
    baseline = round (np.mean(current_trace[:20]))
    trace_final = (current_trace - baseline)
    traceFinal_inv = (trace_final*-1)
    #print (traceFinal_inv.shape)
    traceFinal_inv = traceFinal_inv.transpose()
    #print(traceFinal_inv)
    peaks = find_peaks(traceFinal_inv, height= 500, distance = 7)
    #print("peak0")
    
    
    #print (len(peaks[0]))
    
    #height = peaks[1]['peak_heights']
    #print (height) 
    #print(peaks)
    #print (height)
    trace_final = trace_final + baseline
    only_good = True
    if only_good == True :
        if 3>len(peaks[0])>1 :
            if ((peaks[0][1] - peaks[0][0])<13) and ( 52 >peaks[0][0]> 46):
                fig = plt.figure(figsize=(10,6)) 
                plt.plot(trace_final)
                plt.legend([i])
                plt.suptitle("peaks position = " + str(peaks[0][0]) + "," + str(peaks[0][1]))
                plt.show()
    else:
        fig = plt.figure() 
        plt.plot(trace_final)
        plt.legend([i])
        #plt.suptitle("peaks position = " + str(peaks[0][0]) + "," + str(peaks[0][1]))
        plt.show()
    #input()


# %%
