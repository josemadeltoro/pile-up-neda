
#%%
from __future__ import absolute_import, division, print_function
from inspect import trace

import pathlib

import os
from trace import Trace

# manually specify the GPUs to use
#os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"]="0"

import matplotlib as m
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle as pk
import tables
import datetime

import time

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers import Dense, Activation, Conv1D
import tensorflow.keras.backend as K
from tensorflow.keras.callbacks import TensorBoard, LearningRateScheduler, ReduceLROnPlateau, CSVLogger, ModelCheckpoint, EarlyStopping
print(tf.__version__)

from scipy.io import readsav
from scipy.signal import deconvolve
from scipy.ndimage.interpolation import shift



import matplotlib as mpl
mpl.rcParams.update(mpl.rcParamsDefault)

from matplotlib import colors
import matplotlib
# matplotlib.use('Agg') #comment for jupiter plotting

#TURBO CHARGE YOUR PYTHON -- Make it parallel
import multiprocessing


weights_peak = np.full(52, 0.3)

weights_tail = np.full(180, 1)

weights = np.concatenate((weights_peak, weights_tail))
weights = weights**1

#print (weights)
#plt.plot(weights)
#plt.show()

print (weights.shape)



#print(weights1)

#%%
def convolve(signal, filterProfile): 

    return np.convolve(signal, filterProfile)

def customLoss(yTrue, yPred):
    
    #weights1 = K.constant(weights)
    #weights1 = weights
    # baseline = np.mean()
    return K.mean(K.square(yTrue - yPred)) #* weights1
    #return K.mean(K.square(yTrue - yPred))

def customLoss_MP(yTrue, yPred):

    weights1 = K.constant(weights_MP)
    return K.mean(K.square(yTrue - yPred) * weights1)

def customLoss1(yTrue, yPred):

    weights11 = K.constant(weights[0:40])
    return K.mean(K.square(yTrue - yPred) * weights11)


def load_spectral_profiles(directory,filename):
    hdul     = fits.open(directory+filename)
    spectrum = hdul[0].data
    print(hdul.header)
    return spectrum 
#%%
def build_simple_model1():
  #build the NN needed for the problem accomodating a single
  #convolutional layer and two densely connected hidden layer
  kSzConv1D = 5
  waveNumd  = 232
  model = keras.Sequential([
    layers.Conv1D(50, (3,),
        activation='relu',strides=1,input_shape=[waveNumd,1]),
    layers.Conv1D(40, (3,),
        activation='relu'),
    layers.MaxPooling1D(2),
    layers.Conv1D(25, (3,),
        activation='relu'),
    layers.Conv1D(15, (4,),
        activation='relu'),
    layers.MaxPooling1D(2),
    #layers.GlobalAveragePooling1D(),
    layers.Flatten(),
    layers.Dropout(0.5),
    layers.Dense(40,activation=tf.nn.relu),
    layers.Dense(40,activation=tf.nn.relu),  
    #layers.GlobalAveragePooling1D(),
    #layers.Dense(20, activation='sigmoid'),    
    #layers.Dense(10, activation='sigmoid'),
    layers.Dense(waveNumd)])
  optimizer = tf.keras.optimizers.Adam(lr=0.001, beta_1=0.9,
          beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
  #optimizer = tf.keras.optimizers.Adagrad(learning_rate=0.1,
  #                                          initial_accumulator_value=0.1, epsilon=1e-07)


  model.compile(loss=customLoss,
                optimizer=optimizer,
                metrics=['mean_absolute_error', 'mean_squared_error'])
  return model

def build_conv_model():
  #build the NN needed for the problem accomodating a single
  #convolutional layer and two densely connected hidden layer
  kSzConv1D = 5
  waveNumd  = 232
  model = keras.Sequential([
    layers.Conv1D(15, (3,),
        activation='relu', strides=1, input_shape=[waveNumd, 1]),
    layers.Conv1D(30, (5,),
        activation='relu'),
    layers.MaxPooling1D(2),
    layers.Conv1D(15, (3,),
        activation='relu'),
    layers.Conv1D(25, (3,),
        activation='relu'),
    #layers.MaxPooling1D(2),
    #layers.GlobalAveragePooling1D(),
    layers.Flatten(),
    layers.Dropout(0.5),
    #layers.Dense(30,activation=tf.nn.relu),
    layers.Dense(30,activation=tf.nn.relu),  
    #layers.GlobalAveragePooling1D(),
    #layers.Dense(20, activation='sigmoid'),    
    #layers.Dense(10, activation='sigmoid'),
    layers.Dense(waveNumd)])
  #optimizer = tf.keras.optimizers.Adam(learning_rate=0.001, beta_1=0.9,
  #        beta_2=0.999, epsilon=0.1, decay=0.0, amsgrad=False)
  #optimizer = tf.keras.optimizers.SGD(lr=0.01, nesterov=True)
  optimizer = tf.keras.optimizers.Adam(learning_rate=0.1, beta_1=0.9, #original
          beta_2=0.999, epsilon=0.1, decay=0.0, amsgrad=False)       
  #optimizer = tf.keras.optimizers.Adagrad(learning_rate=0.1,
  #                                          initial_accumulator_value=0.1, epsilon=1e-07)
  model.compile(loss='mean_squared_error',# # customLoss
                optimizer=optimizer,
                metrics=['mean_absolute_error', 'mean_squared_error'])
  return model

def build_conv_model2():
    #build the NN needed for the problem accomodating a single
    #convolutional layer and two densely connected hidden layer
    kSzConv1D = 3
    waveNumd  = 232
    y1 = layers.Input(shape=[waveNumd,1])
    y = layers.Conv1D(5, (kSzConv1D, ), activation='relu')(y1)
    y = layers.MaxPooling1D(2)(y)
    y = layers.Conv1D(10, (kSzConv1D, ), activation='relu')(y)
    y = layers.MaxPooling1D(2)(y)
    y = layers.Conv1D(20, (kSzConv1D, ), activation='relu')(y)
    y = layers.MaxPooling1D(2)(y)
    #y = layers.Dense(100,activation='relu')(y)
    y = layers.UpSampling1D()(y)
    y = layers.Conv1D(20, (kSzConv1D, ), activation='relu')(y)
    #y = layers.Dropout(0.2)(y)
    y = layers.UpSampling1D()(y)
    y = layers.Conv1D(10, (kSzConv1D, ), activation='relu')(y)
    y = layers.UpSampling1D()(y)
    y = layers.Conv1D(5, (kSzConv1D, ), activation='relu')(y)
    #layers.GlobalAveragePooling1D(),
    #y = layers.Flatten()(y)
    #y = layers.Dropout(0.5)(y)
    y = layers.Flatten()(y)
    y = layers.Dense(waveNumd, activation=tf.nn.relu)(y)
    y2 = layers.Add()([y, y1[:,  :, 0]])
    y = layers.Dense(waveNumd, activation=tf.nn.relu)(y2)

    #layers.GlobalAveragePooling1D(),
    #layers.Dense(20, activation='sigmoid'),    
    #layers.Dense(10, activation='sigmoid'),
    
    x = layers.Dense(waveNumd, activation='linear')(y2)
    optimizer = tf.keras.optimizers.Adam(lr=0.1, beta_1=0.9,# lr= 1e-3
            beta_2=0.999, epsilon=0.1, decay=0.1, amsgrad=False) #1e-8
    model = keras.models.Model(inputs=y1, outputs=x)
    model.compile(loss=customLoss, optimizer=optimizer, # 'mean_squared_error'
                  metrics=['mean_absolute_error', 'mean_squared_error'])
    return model

def build_conv_model_MP(waveNumd, ratio):
    '''
    build the NN needed for the problem accomodating a single
    convolutional layer and two densely connected hidden layer
    '''
    kSzConv1D = 3
    # waveNumd  = 40
    
    y1 = layers.Input(shape=[int(waveNumd/ratio),1])
    y = layers.Conv1D(5,  (kSzConv1D, ))(y1)
    y = layers.BatchNormalization()(y)
    y = layers.Activation('relu')(y)
    y = layers.MaxPooling1D(2)(y)
    y = layers.Conv1D(10, (kSzConv1D, ))(y)
    y = layers.BatchNormalization()(y)
    y = layers.Activation('relu')(y)
    y = layers.MaxPooling1D(2)(y)
    #y = layers.Conv1D(20, (kSzConv1D,), activation='relu')(y)
    #y = layers.MaxPooling1D(2)(y)
    # y = layers.Conv1D(20, (kSzConv1D, ), activation='relu')(y)
    # y = layers.Dense(waveNumd,activation='relu')(y)
    #y = layers.UpSampling1D()(y)
    #y = layers.Conv1D(20,(kSzConv1D,), activation='relu')(y)
    # y = layers.Dropout(0.2)(y)
    # y = layers.Dropout(0.2)(y)
    y = layers.UpSampling1D()(y)
    y = layers.Conv1D(10,(kSzConv1D,))(y)
    y = layers.BatchNormalization()(y)
    y = layers.Activation('relu')(y)
    y = layers.UpSampling1D()(y)
    y = layers.Conv1D(5,(kSzConv1D,))(y)
    y = layers.BatchNormalization()(y)
    y = layers.Activation('relu')(y)
    # layers.GlobalAveragePooling1D(),
    # y = layers.Flatten()(y)
    y = layers.Flatten()(y)
    # y = layers.Dense(waveNumd, activation='sigmoid')(y)
    x = layers.Dense(waveNumd,activation='linear')(y)
    
    optimizer = tf.keras.optimizers.Adam(lr=0.001, beta_1=0.9,
            beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model2 = keras.models.Model(inputs=y1, outputs=x)
    model2.compile(loss=customLoss_MP,
                  optimizer=optimizer,
                  metrics=['mean_absolute_error', 'mean_squared_error'])

    return model2

def build_dense_model():
    waveNumd  = 100
    numDense1 = 256
    y1 = layers.Input(shape=[waveNumd,1])
    y = layers.Flatten()(y1)
    y = layers.Dense(numDense1,activation='relu')(y)
    #y = layers.Dense(numDense1,activation='relu')(y)
    y = layers.Dense(numDense1,activation='relu')(y)
    y = layers.Dropout(0.2)(y)
    y = layers.Dense(numDense1,activation='relu')(y)
    y = layers.Dense(numDense1,activation='relu')(y)
    #y = layers.Dense(256,activation='relu')(y)
    #y = layers.Flatten()(y)
    y = layers.Dense(waveNumd,activation='sigmoid')(y)
    y = layers.Add()([y,y1[:,:,0]])
    x = layers.Dense(waveNumd,activation='linear')(y)
    
    optimizer = tf.keras.optimizers.Adam(lr=0.001, beta_1=0.9,
            beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model = keras.models.Model(inputs=y1, outputs=x)
    model.compile(loss=customLoss,
                  optimizer=optimizer,
                  metrics=['mean_absolute_error', 'mean_squared_error'])
    
    return model


def Lorentzian(x0,x,gamma):
    x2  = (x0-x)*(x0-x)
    exp = gamma/(np.pi*(x2+gamma*gamma))
    return exp


def Gaussian(x,mu,sigma):
    return np.exp((-1)*(x-mu)*(x-mu)/(2*sigma*sigma))/np.sqrt(2*np.pi*sigma*sigma)


#%%
#data = pd.read_hdf('/nfs/neutron-ml/test2.h5')
#data = pd.read_pickle('/nfs/neutron-ml/test_ch0_merged_3M_events.pickle')

#data = pd.read_pickle('/nfs/neutron-ml/test_ch1_merged_3M_events.pickle')

#data = pd.read_pickle('/nfs/neutron-ml/test_ch0_merged_9Mevents_2nd_delayed.pickle')
#data = pd.read_pickle('/nfs/neutron-ml/test_ch0_17_pileup_events.pickle')

#data = pd.read_pickle('/nfs/neutron-ml/test_ch2_11_pileup_events.pickle')
#data = pd.read_pickle('/nfs/neutron-ml/test_allchannels_max12delay_pileup_events.pickle')
data = pd.read_pickle('/nfs/neutron-ml/data_only_pile-up_noNUMEXO.pickle')

data.info(verbose=True)

#trace1=np.array([x for x in data['Trace1']]).reshape(-1,232,1).astype('float32')
#trace2=np.array([x for x in data['Trace2']]).reshape(-1,232,1).astype('float32')
traceFinal=np.array([x for x in data['Trace']]).reshape(-1,232,1).astype('float32')




#%%
# train, validation, test
# train = 0.6
# val = 0.2
# test = 1-train-val

# assert train+val+test==1.0

# trace1_train = trace1[:int(len(trace1)*train)]
# trace1_val = trace1[int(len(trace1)*train):int(len(trace1)*train)+int(len(trace1)*val)]
# trace1_test = trace1[int(len(trace1)*train)+int(len(trace1)*val):]

# trace2_train = trace2[:int(len(trace2)*train)]
# trace2_val = trace2[int(len(trace2)*train):int(len(trace2)*train)+int(len(trace2)*val)]
# trace2_test = trace2[int(len(trace2)*train)+int(len(trace2)*val):]

# traceFinal_train = traceFinal[:int(len(traceFinal)*train)]
# traceFinal_val = traceFinal[int(len(traceFinal)*train):int(len(traceFinal)*train)+int(len(traceFinal)*val)]
# traceFinal_test = traceFinal[int(len(traceFinal)*train)+int(len(traceFinal)*val):]



from IPython.display import Image 
from tensorflow.keras.models import load_model

# path_model = "models/model_conv2_model_500epoch_bigdataset_lr01"

# loading_model = True

# if loading_model :
#     model = load_model(path_model)
#     print ("loading trained model = " + path_model)
# else:
#     model = build_conv_model2()

#model = build_simple_model1()

#model = build_dense_model()

# model.summary()
# tf.keras.utils.plot_model(model, to_file="model.png", show_shapes=True)
#Image('model.png')
# model.eval()



#%%

import keras.losses
keras.losses.customLoss = customLoss
#path_model = "models/model_out2_500epochbatch1024_lr01"
#path_model = "models/model_out2_700epochbatch1024_lr01"
path_model = "models/model_out2_1000epochbatch1024_lr01_newdataset"
#path_model = "models/model_out2_epochbatch1024_lr01_newdataset_addingfrompeaks_weights"                     
model = tf.keras.models.load_model(path_model, custom_objects={'Loss':customLoss})
#%%
# for evt in range(10):
#     eventNumber = 232*evt
for i in range(26, 27):
    eventNumber = i
    #eventNumber = (40*i) + i +5
    fig = plt.figure(figsize=(10,6), dpi=600)      
    #plt.xlabel("samples")
    #plt.ylabel("value")
    #plt.ylim(8000,15000)
    #plt.plot(np.array(range(232)),traceFinal[eventNumber].reshape(232), linestyle=':')#, s=1)
    #new_trace2 = trace2[eventNumber].reshape(232)
    #plt.plot(np.array(range(232)),trace2[eventNumber].reshape(232))
    #trace2_shift = np.roll(new_trace2, i+2+3)
    #plt.plot(np.array(range(232)),trace2_shift)#, s=1)
    # plt.show()
    # fig.savefig('TraceFinal.png')

    # fig = plt.figure()      
    #plt.xlabel("samples")
    #plt.ylabel("value")
    #plt.ylim(8000,15000)
    plt.plot(np.array(range(232)),traceFinal[eventNumber].reshape(232))#, s=1)
    #print (trace1[eventNumber])
    # plt.show()
    # fig.savefig('Trace1.png')


    #plt.xlabel("samples")
    #plt.ylabel("value")
    #plt.ylim(10000,15000)
    #plt.plot(np.array(range(232)),trace2[eventNumber].reshape(232))


    result, result2 = model.predict(traceFinal[eventNumber].reshape(1,232,1))
    # fig = plt.figure()      
    plt.xlabel("samples")
    plt.ylabel("value")
    #plt.ylim(8000,15000)
    plt.plot(np.array(range(232)),result.reshape(232), linestyle='--')#, s=1)
    plt.plot(np.array(range(232)),result2.reshape(232), linestyle='--')

    plt.suptitle("Event Number " + str(eventNumber))

    plt.legend(['Input','Prediction 1st signal','Prediction 2nd signal'])
    plt.title("Channel = " + str(data.loc[i]["Channel"]) + "    TimeStamp = " + str(data.loc[i]["Timestamp"]))
    plt.show()
    fig.savefig('.png')


    fig1 = plt.figure(figsize=(10,6))
    plt.plot(np.array(range(232)),result.reshape(232), linestyle='--')#, s=1)
    plt.xlabel("samples")
    plt.ylabel("value")
    plt.legend(['First reconstructed signal'])
    plt.show()      

    fig1 = plt.figure(figsize=(10,6))
    plt.plot(np.array(range(232)),result2.reshape(232), linestyle='--')#, s=1)
    plt.xlabel("samples")
    plt.ylabel("value")
    plt.legend(['Second reconstructed signal'])
    plt.show() 


    fig1 = plt.figure(figsize=(10,6))
    baseline = round (np.mean(result[:20]))
    trace_rec = (result - baseline) + (result2 - baseline) + baseline
    plt.plot(np.array(range(232)),traceFinal[eventNumber].reshape(232))#, s=1)
    plt.plot(np.array(range(232)),trace_rec.reshape(232), linestyle='--')
    
    plt.xlabel("samples")
    plt.ylabel("value")
    plt.legend(['Input signal','Combining 2 results'])
    plt.show()      




# plt.savefig('manu_test.png')

#%%
eventNumber = 402190
result, result2 = model.predict(traceFinal[eventNumber].reshape(1,232,1))

fig = plt.figure() 
plt.xlabel("samples")
plt.ylabel("value")
#plt.ylim(0,)

#traceFinal_inv = (traceFinal[eventNumber]*-1)+14700
#plt.plot(np.array(range(232)),traceFinal[0].reshape(232)-result.reshape(232))#, s=1)

plt.plot(np.array(range(232)),traceFinal[eventNumber].reshape(232))


#from scipy.signal import find_peaks

#print (traceFinal_inv.shape)
#traceFinal_inv = traceFinal_inv.transpose()
#print(traceFinal_inv[0])
#peaks = find_peaks(traceFinal_inv[0], height= 200, distance = 2)
#height = peaks[1]['peak_heights']
#print (height) 
#print(peaks)

#plt.plot(np.array(range(232)),traceFinal_inv.reshape(232))




#result2 = traceFinal[eventNumber].reshape(232)-result.reshape(232)
#result2 = result2.transpose()
trace_test=traceFinal[eventNumber]
baseline = round (np.mean(trace_test[:20]))
#result2 = result2 + baseline

#trace2_shift = np.roll(trace2[eventNumber], 8)

plt.plot(np.array(range(232)),trace1[eventNumber].reshape(232), linestyle = "--", alpha = 0.7)
plt.plot(np.array(range(232)),trace2[eventNumber].reshape(232), linestyle = "--", alpha = 0.7)

#plt.legend(['Signal with pile-up', 'First signal', 'Second signal'])
plt.show()
#%%
fig = plt.figure() 
plt.xlabel("samples")
plt.ylabel("value")
#plt.ylim(0,)


plt.plot(np.array(range(232)),result.reshape(232),linestyle='--')#, s=1)
plt.plot(np.array(range(232)),trace1[eventNumber].reshape(232), alpha = 0.5)
plt.legend(['Pred 1st signal', '1st signal'])
#plt.show()


fig = plt.figure()
plt.plot(np.array(range(232)),result2.reshape(232),linestyle='--')
# fig = plt.figure()      
plt.xlabel("samples")
plt.ylabel("value")
#plt.ylim(0,15000)
#plt.plot((np.array(range(232)),traceFinal[42683].reshape(232)-result.reshape(232)))#, s=1)

plt.plot(np.array(range(232)),trace2[eventNumber].reshape(232), alpha = 0.5)#, s=1)
plt.legend([ 'Pred 2nd signal','2nd signal'])
#print (trace1[eventNumber])
plt.show()
# fig.savefig('Trace1.png')



# fig.savefig(path_model + '/Comparison_conv2_500epochs_bigdataset_lr01.png')
# %%


result1, result2 = model.predict(traceFinal[2091].reshape(1,232,1))
trace_test=traceFinal[2091]
result1 = result1.transpose()

baseline = round (np.mean(trace_test[:20]))

##result = np.array(range(232)),result.reshape(232)

trace_test = trace_test - baseline

result_test = result1 - baseline
#result_test = round(result_test)
print(result_test)

removing_test = trace_test - result_test

# %%

fig = plt.figure() 
plt.xlabel("samples")
plt.ylabel("value")
#plt.ylim(0,15000)
plt.plot(trace_test, linestyle='--')
#plt.plot (result_test, linestyle=':')
plt.plot (removing_test)
plt.show()


# %%
